<?php
/**
 * Clock Plugin: Shows realtime clock
 * 
 * @file       clock/syntax.php
 * @brief      Show a clock in wikipage
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author     Luis Machuca B. <luis.machuca [at] gulix.cl>
 * @version    1.4
 * @date       2010-03-30
 *
 *
 *  This plugin's purpose is to display a clock using both 
 *  CSS and JavaScript techniques normally available.
 *
 *   For a live test point a decent web browser to my wiki.
 *  http://informatica.temuco.udelmar.cl/~lmachuca/dokuwiki-lucho/
 *
 *  Greetings.
 *        - Luis Machuca Bezzaza a.k.a. 'ryan.chappelle'
 */


if(!defined('DW_LF')) define('DW_LF',"\n");
 
if(!defined('DOKU_INC')) 
define('DOKU_INC',realpath(dirname(__FILE__).'/../../').'/');
if(!defined('DOKU_PLUGIN')) 
define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'syntax.php');
 
/**
 * All DokuWiki plugins to extend the parser/rendering mechanism
 * need to inherit from this class
 */
class syntax_plugin_clock extends DokuWiki_Syntax_Plugin {
 
    /**
     * return some info
     */
    function getInfo(){
        return array(
            'author' => 'Luis Machuca Bezzaza',
            'email'  => 'luis.machuca [at] gulix.cl',
            'date'   => '2010-03-30',
            'name'   => 'Clock Plugin',
            'desc'   => 'Displays a realtime clock',
            'url'    => 'http://www.dokuwiki.org/plugin:clock',
        );
    }
 
    /**
     * What kind of syntax are we?
     */
    function getType(){
        return 'substition';
    }
 
    /**
     * What can we Do?
     */
    function getAllowedTypes() { 
        return array(); 
    }
 
    /**
     * Where to sort in?
     */
    function getSort(){
        return 290;
    }
 
    /**
     * What's our code layout?
     */    
    function getPType(){ 
        return 'block'; 
    }
 
    /**
     * How do we get to the lexer?
     */
    function connectTo($mode) {
         $this->Lexer->addSpecialPattern (
         '^\{\{clock\}\}$', $mode, 'plugin_clock');
 
    }
 
    /**
     * Handle the match
     */
    function handle($match, $state, $pos, &$handler){
 
        $data= array();
        // get the clock ID, JS fallback and infopage
        $theCS= $this->getConf('clock_style');
        $theJS= $this->getConf('nojs_fallback');
        // if 'nojs_fallback' is set, we get the time from the server
        // if 'clock_infopage' contains a link, we convert it

        /* compose the data array */
        $data['style']   = $theCS;
        $data['text']    = $theJS ? date('H:i:s') : $theCS;
        
        /* Are we ready yet? */

        return $data;
    }  
 
    /**
     * Create output
     */
    function render($mode, &$renderer, $data) {
      /* get the data from $data */
      static $wasrendered= false;

      if ($mode == 'xhtml') {
        if (!$wasrendered) {
          $renderer->doc .= '<div id="'. $this->_get_clock_object_ID(). '">' ;
          $renderer->doc .= $this->_clock_createblock_html($data);
          /* if ($this->getConf('helpbar') )  $renderer->doc .= $this->_get_clock_helpbar(); */
          $renderer->doc .= '</div><!-- end clock-->';
          $wasrendered= true;
          }
        else {
          $renderer->doc.= '<p><a href="#'. $this->_get_clock_object_ID(). '" class="wikilink" title="Go To Clock">⌚ clock</a></p>';
          }
        return true;
        }
      else if ($mode == 'odt') {
        return false; // may be implemented in the future
        }
      else if ($mode == 'text') {
        $text= ' ['. $this->_get_clock_object_ID(). ' '. date('H:i'). '] ';
        $renderer->doc .= $text;
        return true;
        }
      /* That's all about rendering that needs to be done, at the moment */
      return false;
    }

    /**
     * From this point onwards, local (helper) functions are implemented
     */

    /** 
     * @fn          dw_clock_object_ID
     * @brief       returns ID for the clock object
     *  This function sets the name used for the ID of the clock object.
     *  It is intended to return a somewhat unique name.
     *  If you want to change it, you will also need to update 
     *  the #id tags in the 'style.css' file. 
     */
    function _get_clock_object_ID () {
      return $this->getConf('clock_id');
      }

    /**
     * @fn          dw_clock_helpbar
     * @brief       Returns the contents of the help bar
     */
    function _get_clock_helpbar () {
      $p.= '<p class="helpbar" >';
      $link= $this->getConf('clock_infopage');
      if (empty($link) ) { // point to plugin page by default
        $info= '[[doku>plugin:clock|Clock Plugin]]';
        }
      else {
        $info= "[[$link|Info]]";
        }
      $info= p_render('xhtml', p_get_instructions($info), $info);
      $info= substr($info, 4, -5); // remove <p>, </p>
      $p.= $info;
      $p.= '</p>';       
      return $p;
      }


    /**
     * @fn          _clock_createblock_html
     * @brief       Creates the HTML code for the clock object.
     */
    function _clock_createblock_html($data) {
        $theStyle   = $data['style'];
        $theText    = $data['text'];
        $theDoLink  = $data['dolink'];
        $theTarget  = $data['target'];

        $codetext.= '<div id="clock_face" class="'. $theStyle. ' face">'. 
                    $theText.DW_LF;

        if ($theDoLink) {

          }
        
        $codetext.= '</div>'. DW_LF;
        return $codetext;
     }

}


