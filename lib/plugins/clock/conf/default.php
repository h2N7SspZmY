<?php
/**
 * Default configuration for clock plugin
 *
 * @license:    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author:     Luis Machuca <luis.machuca@gulix.cl>
 */


/* default clock object ID 
  Note 1: this can't be changed from Config Manager
  Note 2: if you change this value, you must change the IDs for 
          the CSS styles as well! (Javascript will update itself)
 */
$conf['clock_id']        = 'dw_clock_object';
/* clock_style controls the CSS style used */
$conf['clock_style']     = 'default';
/* clock_infopage controls the wikilink available for help/tooltip 
   if it is empty, it will point to the plugin page at DokuWiki
*/
$conf['clock_infopage']  = '';
/* font_fallback controls the CSS family used by default for the clock face 
   (this property is currently unused)
*/
$conf['font_fallback']   = 'monospace';
/* nojs_fallback specifies behaviour when JavaScript is not enabled */
$conf['nojs_fallback']   = false;
/* helpbar: this property controls whether the helpbar will be visible */
$conf['helpbar']         = true;
/* toolbar: this property controls whether the toolbar will be visible */
$conf['toolbar']         = false;



