/**
 * Clock Plugin: Shows realtime clock
 * 
 * @file       clock/script.js
 * @brief      Javascript for the Clock Plugin
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author     Luis Machuca B. <luis.machuca [at] gulix.cl>
 * @version    1.4
 * @date       2010-03-30
 * 
 *  This file contains the timer script for the clock plugin.
 */

// id of the clock object. 
//DONT CHANGE THIS UNLESS YOU KNOW WHAT YOU ARE DOING!
var jsclock_id= "clock_face";


// the Clock Plugin Timer object
var dwClockTimer = new dwclock_();
// the DOM object used for visualization
var dwClockDOMObject;


/** 
 * @fn     dwClock
 * @brief  Constructs the timer used by the plugin
 */
function dwclock_ () {
  this.hh = 0;
  this.uh = 0;
  this.mm = 0;
  this.ss = 0;

  this.update = dwclock_update;
  }


/**
 * @fn    dwclock_update
 * @brief This function updates the clock data
 */
function dwclock_update () {

  // tick the clock
  var cT  = new Date();
  var Ahh = cT.getHours();
  var Amm = cT.getMinutes();
  var Ass = cT.getSeconds();

  // format it as ISO 8601 text

  if (Ahh<=9 && Ahh>=0)
    Ahh= "0" + Ahh;
  if (Amm<=9 && Amm>=0)
    Amm= "0" + Amm;
  if (Ass<=9 && Ass>=0)
    Ass= "0" + Ass;

  timetext= " " + Ahh + ":" + Amm + ":" + Ass + " ";

  // assign the text to the element
  // whose id is that of the 'jsclock_id' variable

  //var theElement= document.getElementById (jsclock_id);

  dwClockDOMObject.innerHTML= timetext;

  // do the timeout
  }

/**
 * @fn    dwclock_update
 * @brief This function makes the clock tick
 */
function dwclock_tick () {
  dwClockTimer.update();
  setTimeout(dwclock_tick, 500);
  }

addInitEvent( function() {
  dwClockDOMObject = $(jsclock_id);
  //dwClockTimer     = new dwclock_ ();
  } );

addInitEvent(dwclock_tick);
// end of clock/script.js

