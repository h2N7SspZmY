<?php
/**
 * Spanish language for jsclock plugin
 *
 * @license:    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author:     Luis Machuca <luis.machuca@gulix.cl>
 */

$lang['clock_style']     = "Nombre del estilo en CSS utilizado para decorar el reloj; por defecto asume <tt>clock_default</tt> que viene incluido con el plugin. <br/>Otras clases pueden ser definidas copiando el ejemplo a <tt>conf/userstyle.css</tt> y etidando a conveniencia.";

$lang['clock_infopage']  = "Si se especifica, es un v&iacute;nculo a la <em>p&aacute;gina wiki</em> especificada.<br/>Por ejemplo, para explicar la caracteristica a los visitantes del sitio.";

$lang['font_fallback']   = "El nombre de la &laquo;<em>familia</em>&raquo; de la utilizada para la fuente.<br/>De momento esta caracter&iacute;stica est&aacute; <strong>deshabilitada</strong>.";

$lang['nojs_fallback']   = "Especifica qu&eacute; hacer si JavaScript no se encuentra habilitado:<br/><ul><li><b>Activado</b>:, se muestra la hora del servidor de manera est&aacute;tica.</li><li><b>Desactivado</b>: se muestra solamente un mensaje con el <tt>id</tt> del reloj.</li></ul>";

