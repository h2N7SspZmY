<?php
/**
 * English language for jsclock plugin
 *
 * @license:    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author:     Luis Machuca <luis.machuca@gulix.cl>
 */

$lang['clock_style']     = "CSS style used to decorate the clock object; defaults to <tt>clock_default</tt> included with the plugin. <br/>Alternate classes can be defined by copying the default and editing as desired into <tt>conf/userstyle.css</tt>.";

$lang['clock_infopage']  = "If set, a link to the specified <em>wiki page</em> is displayed aside the clock.<br/>Useful to, eg.: explain the feature to your users.";

$lang['font_fallback']   = "The &laquo;<em>generic family</em>&raquo; font that is applied as a fallback.<br/><strong>This feature is currently unavailable</strong>.";

$lang['nojs_fallback']   = "Controls behaviour when JavaScript is not available:<br/><ul><li>If <b>set</b>, the time returned by the server will be displayed as normal, static text.</li><li>If <b>unset</b>, the name of the clock object's <tt>id</tt> will be displayed instead of the clock.</li></ul>";

