<?php
/**
 * Options for the chart plugin
 *
 * @author Ikuo Obataya <I.Obataya@gmail.com>
 */

$lang['default_width']   = 'デフォルトの幅';
$lang['default_height']  = 'デフォルトの高さ';
$lang['default_bgcolor'] = 'デフォルトの背景色';
$lang['license']         = 'XML/SWF Chartsライセンス';
