<?php
/**
 * Options for the chart plugin
 *
 * @author Ikuo Obataya <I.Obataya@gmail.com>
 */

$lang['default_width']   = 'Default slideshow width';
$lang['default_height']  = 'Default slideshow height';
$lang['default_bgcolor'] = 'Default background color';
$lang['license']         = 'License code for XML/SWF Charts';
