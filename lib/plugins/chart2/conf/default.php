<?php
/**
 * Options for the chart plugin
 *
 * @author Ikuo Obataya <I.Obataya@gmail.com>
 */

$conf['default_width']   = 320;
$conf['default_height']  = 240;
$conf['default_bgcolor'] = "#FFFFFF";
$conf['license']         = "";
