<?php
/**
 * Options for the chart plugin
 *
 * @author Ikuo Obataya <I.Obataya@gmail.com>
 */

$meta['default_width']   = array('numeric');
$meta['default_height']  = array('numeric');
$meta['default_bgcolor'] = array('string');
$meta['license']         = array('string');
