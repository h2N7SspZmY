<?php
/*
description : Display XML/SWF Charts ver.5
author      : Ikuo Obataya
email       : i.obataya@gmail.com
lastupdate  : 2008-10-30
license     : GPL 2 (http://www.gnu.org/licenses/gpl.html)
*/
if(!defined('DOKU_INC')) define('DOKU_INC',realpath(dirname(__FILE__).'/../../').'/');
  require_once(DOKU_INC.'inc/init.php');
if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
  require_once(DOKU_PLUGIN.'syntax.php');

class syntax_plugin_chart2 extends DokuWiki_Syntax_Plugin {
  var $swfLoc    = '';
  var $swfLibLoc = '';
  var $template  = '';
  // Constructor
  function syntax_plugin_chart2(){
    $this->swfLoc = DOKU_BASE.'lib/plugins/chart2/maani.us/charts';
    $this->swfLibLoc = DOKU_BASE.'lib/plugins/chart2/maani.us/charts_library';
    $this->setTemplate();
  }
  function getInfo(){
    return array(
      'author' => 'Ikuo Obataya',
      'email'  => 'I.Obataya@gmail.com',
      'date'  => '2008-10-30',
      'name'  => 'XML/SWF Charts Plugin',
      'desc'  => 'Create SWF Charts by www.maani.us
      <chart2 (filename)> script </chart2>',
      'url'  => 'http://wiki.symplus.co.jp/computer/en/chart2_plugin',
    );
  }
  function getType(){  return 'protected';  }
  function getSort(){  return 916;  }
  function connectTo($mode) {$this->Lexer->addEntryPattern('<chart2(?=.*?>.*?</chart2>)',$mode,'plugin_chart2');}
  function postConnect(){$this->Lexer->addExitPattern('</chart2>','plugin_chart2');}

  function handle($match, $state, $pos) {
    switch ($state) {
      case DOKU_LEXER_ENTER :    return array($state);
      case DOKU_LEXER_UNMATCHED :return array($state,$match,$pos);
      case DOKU_LEXER_EXIT :     return array($state);
    }
    return array();
  }
  function render($mode, &$renderer, $data){
    if ($mode!='xhtml') return;
    global $ID;
    global $conf;
    @list($state, $match,$pos) = $data;
    switch ($state) {
      case DOKU_LEXER_ENTER:break;
      case DOKU_LEXER_UNMATCHED:
        $i = strpos($match,">");
        if($att_idx<0) return array();
        $att_str = trim(substr($match,0,$i));
        $chart_xml = substr($match,$i+1);

        $att = explode(" ",$att_str);
        $tryFN = preg_replace("/({{)?([^|}]+).*(}})?/",'$2',$att[0]);
        if(strpos($tryFN,"http")!==false){
          /*
           * external xml file
           */
          $source = "xml_source=".$tryFN;
          $idx=1;
        }else{
          /*
           * internal xml/data
           */
          $filename = mediaFN($tryFN);
          if(is_dir($filename) || !file_exists($filename)){
              $filename="";
              $xml_data= strtr($chart_xml,array("\n"=>"","\t"=>""));
              $xml_data= preg_replace("/>\s+</","><",$xml_data);
              $source = "xml_data=".$xml_data;
              $idx=0;
          }else{
            //$source = "xml_source=".ml($tryFN,"",true,"",true);
            $source = "xml_source=/~kssilveira/site/lib/exe/fetch.php?media=".$tryFN;
            $idx=1;
          }
        }
        $width   = (!empty($att[$idx])) ? $att[$idx] : $this->getConf('default_width');$idx++;
        $height  = (!empty($att[$idx])) ? $att[$idx] : $this->getConf('default_height');$idx++;
        $oid     = (!empty($att[$idx])) ? $att[$idx] : 'chart'.$pos;$idx++;
        $bgcolor = (!empty($att[$idx])) ? $att[$idx] : $this->getConf('default_bgcolor');$idx++;
        $align   = (!empty($att[$idx])) ? $att[$idx] : '';

        $renderer->doc.=sprintf($this->template,$width,$height,$this->swfLoc,$this->swfLoc,
                        "library_path=".$this->swfLibLoc."&".$source,$oid,$oid,$bgcolor,$align);
        break;
      case DOKU_LEXER_EXIT:break;
    }
    return true;
  }
  
  function setTemplate(){
    $this->template = <<<EOT
    <script language="JavaScript" type="text/javascript">
    <!--
    var requiredMajorVersion = 9;
    var requiredMinorVersion = 0;
    var requiredRevision = 45;
    if (AC_FL_RunContent == 0 || DetectFlashVer == 0) {
      alert("This page requires AC_RunActiveContent.js.");
    } else {
      var hasRightVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);
      if(hasRightVersion) {
        AC_FL_RunContent(
          'codebase', 'http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,45,0',
          'width',     '%s',
          'height',    '%s',
          'movie',     '%s',
          'src',       '%s',
          'FlashVars', "%s", 
          'id',        '%s',
          'name',      '%s',
          'bgcolor',   '%s',
          'align',     '%s',
          'scale', 'noscale',
          'salign', 'TL',
          'wmode', 'opaque',
          'menu', 'true',
          'allowFullScreen', 'true',
          'allowScriptAccess','sameDomain',
          'quality', 'high',
          'pluginspage', 'http://www.macromedia.com/go/getflashplayer',
          'play', 'true',
          'devicefont', 'false'
          ); 
      } else { 
        var alternateContent = 'This content requires the Adobe Flash Player. '
        + '<u><a href=http://www.macromedia.com/go/getflash/>Get Flash</a></u>.';
        document.write(alternateContent); 
      }
    }
    -->
    </script>
    <noscript><p>This content requires JavaScript.</p></noscript>
EOT;
  }
}
?>
