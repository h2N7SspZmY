<?php
/**
 * Spanish language file for the countdown plugin.
 *
 * @license GPL 2 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author  Luis Machuca B. <luis [dot] machuca [at] gulix [dot] cl>
 *
 * @version 2.1 (2008-03-04)
 *          bugfix: no newline after "?>"
 *          new: today
 * @version 2.0 (2008-02-18)
 *          enhanced functionality
 *
 * @since   2.0
 *
 * Attention: the last characters of the file have to be "?>", no newline or something else
 */

$lang['desc']         = 'Este plugin muestra los días que faltas hasta la fecha específica.';
$lang['wrongformat']  = 'No puede analizarse la fecha: ';
$lang['nodesc']       = 'sin descripción';
$lang['until']        = 'hasta';
$lang['since']        = 'desde';
$lang['today']        = 'Hoy es';
$lang['oneday']       = 'día';
$lang['days']         = 'días';
$lang['outputformat'] = '%d/%m/%Y';
?>
