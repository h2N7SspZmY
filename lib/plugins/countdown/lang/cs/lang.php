<?php
/**
 * English language file for the countdown plugin.
 *
 * @license GPL 2 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author Roman Šilar <roman.silar [at] gmail.com>
 *
 * @version 2.5rc (2009-12-18)
 *          adapted for getLang() usage <Luis Machuca B.>
 *
 * @version 2.1 (2008-03-04)
 *          bugfix: no newline after "?>"
 *          new: today
 * @version 2.0 (2008-02-18)
 *          enhanced functionality
 *
 * @since   2.0
 *
 * Attention: the last characters of the file have to be "?>", no newline or something else
 */

$lang['desc']         = 'Tento plugin zobrazuje dny do určité data.';
$lang['wrongformat']  = 'Datum nelze zpracovat: ';
$lang['nodesc']       = 'bez popisku';
$lang['until']        = 'do';
$lang['since']        = 'od';
$lang['today']        = 'Dnes je';
$lang['oneday']       = 'den';
$lang['days']         = 'dny(í)';
$lang['outputformat'] = '%d/%m/%Y';
?>
