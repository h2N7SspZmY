<?php
/**
 * Arquivo de linguagem para Português-Brasileiro do plugin countdown.
 *
 * @license GPL 2 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author  Adirson Maguila <maguila2000 [at] ig.com.br>
 *
 * @version 2.5rc (2009-12-18)
 *          adapted for getLang() usage (Luis Machuca B.)
 *
 * @version 2.1 (2008-03-04)
 *          bugfix: no newline after "?>"
 *          new: today
 * @version 2.0 (2008-02-18)
 *          enhanced functionality
 *
 * @since   2.0
 *
 * Attention: the last characters of the file have to be "?>", no newline or something else
 */

$lang['desc']         = 'Este plugin mostra os dias passados até uma data específica.';
$lang['wrongformat']  = 'A data não foi corretamente lida: ';
$lang['nodesc']       = 'sem descrição';
$lang['until']        = 'até';
$lang['since']        = 'desde';
$lang['today']        = 'Hoje é';
$lang['oneday']       = 'dia';
$lang['days']         = 'dias';
$lang['outputformat'] = '%d/%m/%Y';
?>
