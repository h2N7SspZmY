<?php
/**
 * Configuration-manager metadata for countdown plugin
 *
 * @license:    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author:     Luis Machuca <luis.machuca@gulix.cl>
 * @version     2.5rc
 */

$meta['include_date']    = array('onoff');
$meta['use_today']       = array('onoff');
$meta['with_hours']      = array('onoff');

