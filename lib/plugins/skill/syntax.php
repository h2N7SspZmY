<?php
 
if(!defined('DOKU_INC')) define('DOKU_INC',realpath(dirname(__FILE__).'/../../').'/');
if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'syntax.php');
 
/**
 * All DokuWiki plugins to extend the parser/rendering mechanism
 * need to inherit from this class
 */
class syntax_plugin_skill extends DokuWiki_Syntax_Plugin {
 
    /**
     * return some info
     */
    function getInfo(){
        return array(
            'author' => 'iDo',
            'email'  => 'iDo@woow-fr.com',
            'date'   => '13/10/2005',
            'name'   => 'Skill Plugin',
            'desc'   => 'Add the possibility to show skill level',
            'url'    => 'http://www.dokuwiki.org/plugin:skill',
        );
    }
 
    /**
     * What kind of syntax are we?
     */
    function getType(){
        return 'substition';
    }
 
    /**
     * Where to sort in?
     */
    function getSort(){
        return 107;
    }
 
    /**
     * Connect pattern to lexer
     */
    function connectTo($mode) {
      $this->Lexer->addSpecialPattern("{{skill>[0-9]*/?[0-9]*}}",$mode,'plugin_skill');
    }
 
    /**
     * Handle the match
     */
    function handle($match, $state, $pos, &$handler){
        $match = substr($match,8,-2); // Strip markup
		$match=split('/',$match); // Strip size
 
        if (!isset($match[1])) $match[1] = $match[0];
		if ($match[0]>$match[1]) $match[1]=$match[0];
        return $match;
    }  
 
    /**
     * Create output
     */
    function render($mode, &$renderer, $data) {
        if($mode == 'xhtml'){       		
            $renderer->doc .= '<span>';
            $renderer->doc .= $this->_Skill($data);
            $renderer->doc .= '</span>';
            return true;
        }
        return false;
    }
 
	function _Skill($d) {
		$mRet='';
		for($i=1;$i<=$d[0];$i++) {
			$mRet.='<img src="lib/plugins/skill/star.png" alt="." style="" />';
		}
		for($i=1;$i<=($d[1]-$d[0]);$i++) {
			$mRet.='<img src="lib/plugins/skill/star.png" alt="." style="filter:alpha(opacity=40);-moz-opacity:0.4;opacity: 0.4;" />';
		}
 
		return $mRet;
	}
}
 
//Setup VIM: ex: et ts=4 enc=utf-8 :
?>
