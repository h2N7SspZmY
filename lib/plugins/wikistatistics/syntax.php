<?php
 /**
 * Info Plugin: Displays information about various DokuWiki internals
 *
 * @license			GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @original author	Paco Avila (Monkiki) <monkiki@gmail.com>
 * @author			Emanuele <emanuele45@interfree.it>
 * @contributor		Thomas <thomas(dot)delhomenie(at)gmail(dot)com>
 * @patched by		Matthieu <matthieu(dot)rioteau(at)skf(dot)com> (11/10/2009)
 *					(2009/11/10)  - Patch correct bad behavior in "bymonth" function where comparison of months considered October (#10) to be lesser than February (#2) because of leading zeros suppression
 *					(2010/01/05)  - Still problems with date comparison -> solved by comparing integer representation of dates
 *								  - Missing double quotes in "histoContribByMonth" function
 *								  - Bad behavior of "toBeCounted" function when no namespace is excluded
 * @patched by		Matthias Grimm <matthiasgrimm(at)users(dot)sourceforge(dot)net> (11/12/2009)
 *					(2009/12/11)  - Patch correct full name not correctly displayed on HoF with non-plain auth system, now all authentication systems are supported + code cleanup
 */

if(!defined('DOKU_INC')) define('DOKU_INC',realpath(dirname(__FILE__).'/../../').'/');
if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'syntax.php');

// Standard inclusions
if(!class_exists('syntax_plugin_charter')){
	if(!class_exists('pData')){
	include(DOKU_PLUGIN.'wikistatistics/pChart/pData.class');
	}
	if(!class_exists('pChart')){
	include(DOKU_PLUGIN.'wikistatistics/pChart/pChart.class');
	}
}

/**
 * All DokuWiki plugins to extend the parser/rendering mechanism
 * need to inherit from this class
 */
class syntax_plugin_wikistatistics extends DokuWiki_Syntax_Plugin {
	var $allChanges = array();
	var $allPagesSizes = array();
	var $localExcludedns = '';
	var $localExcludedns_pattern = '';
	var $localExcludedpg_pattern = '';

	var $excludedNs = '';
	var $excludedNsPattern = '';
	var $excludedpg_pattern = '';

	var $initOpt = '';

	/**
	 * return some info
	 */
	function getInfo(){
		return array(
			'author' => 'Emanuele, Thomas',
			'email'  => 'emanuele45@interfree.it',
			'date'   => '2010-01-24',
			'name'   => 'WikiStatistics',
			'desc'   => 'Display statistics about the Wiki and their users',
			'url'	 => 'http://lacroa.altervista.org/dokucount/',
		);
	}

	/**
	 * What kind of syntax are we?
	 */
	function getType(){
		return 'substition';
	}

	/**
	 * Paragraph Type
	 *
	 * Defines how this syntax is handled regarding paragraphs. This is important
	 * for correct XHTML nesting. Should return one of the following:
	 *
	 * 'normal' - The plugin can be used inside paragraphs
	 * 'block'  - Open paragraphs need to be closed before plugin output
	 * 'stack'  - Special case. Plugin wraps other paragraphs.
	 *
	 * @see Doku_Handler_Block
	 */
	function getPType() {
		return 'block';
	}

	/**
	 * Where to sort in?
	 */
	function getSort(){
		return 210;
	}

	/**
	 * Connect pattern to lexer
	 */
	function connectTo($mode) {
		//$this->Lexer->addSpecialPattern('~~STATS:.*?~~',$mode,'plugin_wikistatistics'); <= first syntax
		//$this->Lexer->addSpecialPattern('~~rSTATS:.*?~~',$mode,'plugin_wikistatistics'); <= second syntax
		$this->Lexer->addSpecialPattern('{{wikistatistics>.*?}}',$mode,'plugin_wikistatistics'); // <= current syntax
	}

	/**
	 * Handle the match
	 */
	function handle($match, $state, $pos, &$handler){

		//convert $match into an array of parameters

		$match = trim($match, '{}');
		$match = substr($match,strpos($match,'>')+1);

		$explodedMatch = explode(' ', $match);

		$params = array();

		for($i = 0; $i < sizeof($explodedMatch); $i++) {
			$param = trim($explodedMatch[$i]);
			$key = substr($param,0,strpos($param,'='));
			$value = substr($param,strpos($param,'=')+1);
			$params[$key] = $value;
		}

		return $params;
	}

	/**
	 * Create output
	 */
	function render($mode, &$renderer, $data) {

		if($mode == 'xhtml')
		{
			$this->varinit($data);

			switch ($data['type']) {
				case 'topcontrib': //HoF
				case 'hof':
					$renderer->doc .= $this->getTopContrib();
					break;
				case 'histocontrib':
					$renderer->doc .= $this->histoContrib();
					break;
				case 'pages':	//Total number of pages
					$renderer->doc .= $this->countPages($this->initOpt['ns']);
					break;
				case 'users':	//Total number of users
					$renderer->doc .= $this->countUsers();
					break;
				case 'pagessizes':	//
					$renderer->doc .= $this->pagesSizes();//TODO add $this->initOpt['ns']
					break;
				case 'hofpagessizes':
					$renderer->doc .= $this->getTopPagesSizes();//TODO add $this->initOpt['ns']
					break;
				case 'topchanged':
					//$renderer->doc .= $this->getTopChanged($this->initOpt['ns']);
					/*
				case 'except':	//Total number of pages except...
					//array_shift($stat);
					$renderer->doc .= $this->countPagesExcept(); //$stat);
					break;
				case 'exceptpattern':	//Total number of pages except a pattern in ns or page name...
					//array_shift($stat);
					$renderer->doc .= $this->countPagesExceptCont(); //$stat);
					break;
					*/
			}
			unset($this->allChanges);
			return true;
		}
		return false;

	}

	/**
	 * Hall of Fame
	 */
	function getTopContrib() {
		global $auth;

		// nb of rows to display
		// if missing, 10 will be taken as default value
		$nbOfRows = (isset($this->initOpt['nbOfRows']) && is_numeric($this->initOpt['nbOfRows'])) ? $this->initOpt['nbOfRows'] : 10;

		$ret = '
	<table class="info_hof inline">
		<caption class="hof_caption">'.$this->getLang('ws_hof').'</caption>
		<tr>
			<th class="centeralign">'.$this->getLang('ws_position').'</th>
			<th class="centeralign">'.$this->getLang('ws_name').'</th>
			<th class="centeralign">'.$this->getLang('ws_editnumb').'</th>
		</tr>';

		$this->getAllChanges();

		$usersedits = array();

		// loop through all changes to count number of edits by user
		foreach($this->allChanges as $singleChange) {
			if ($singleChange['user'] != "" && $this->toBeCounted($singleChange['id'])) {
				if($singleChange['type'] != "D" && $singleChange['type'] != "R" ) {
					$usersedits[$singleChange['user']]++;
				}
			}
		}

		// use full name or pseudo ?
		$useFullName = ($this->initOpt['namecol'] == 'fullname');

		foreach($usersedits as $username => $nbofedits) {
			if($useFullName) {
				// get full user name from auth object
				$info = $auth->getUserData($username);
				$userDisplayName[$username] = (isset($info) && $info) ? hsc($info['name']) : hsc($username);
			} else {
				$userDisplayName[$username] = hsc($username);
			}
		}

		// Sort the data with volume descending, edition ascending
		// Add $data as the last parameter, to sort by the common key
		array_multisort($usersedits, SORT_DESC, $userDisplayName, SORT_ASC);

		foreach ($usersedits as $userid => $edits) {
			$evenodd = $i++ % 2 ? "hof_evenrow" : "hof_oddrow";
			if ($nbOfRows == '-1' || $i <= $nbOfRows) {//$this->getConf('ws_topcontrib')+1 || $this->getConf('ws_topcontrib') == -1) {
				$ret .= '
		<tr class="'.$evenodd.'">
			<td class="hof_row_pos"><b>'.$i.'</b></td>
			<td class="hof_row_name">'.$userDisplayName[$userid].'</td>
			<td class="hof_row_num">'.$edits.'</td>
		</tr>';
			}
		}

		$ret .= '
	</table>';

		return $ret;
	}

	function filterChanges($by='user', $onlyif=false, $andnot=array('type' => 'D', 'type' => 'R')){
		$filter = array();

		// loop through all changes to count number of edits by user
		foreach($this->allChanges as $singleChange) {
			if ($singleChange[$by] != "" && $this->toBeCounted($singleChange['id'])) {
				$add = true;
				if(is_array($onlyif)){
					foreach($onlyif as $key => $value){
						if($singleChange[$key]==$value){
							$add=true;
							break;
						}
					}
				}
				if(is_array($andnot)){
					foreach($andnot as $key => $value){
						if($singleChange[$key]==$value){
							$add=false;
							break;
						}
					}
				}
				if($add)
					$filter[$singleChange[$by]]++;
				//if($singleChange['type'] != "D" && $singleChange['type'] != "R" ) {
				//}
			}
		}
		return $filter;
	}

	/**
	 * Most changed pages
	 */
	function getTopChanged() {
		$this->getAllChanges();
		$edits = $this->filterChanges('id');
		echo "<pre>";
		print_r($edits);
		echo "</pre> count:" . count($edits);
	}

	/**
	 * Edits charts
	 */
	function histoContrib() {
			switch ($this->initOpt['mode']) {
				case 'bymonth':
					return $this->histoContribByMonth();
					break;
				case 'byyear':
					break;
				case 'monthbyday':
					$period = $this->initOpt['period'];
					if($period == '') {
						$dt = getdate();
						$month = $dt['mon'];
						$year = $dt['year'];
					} else {
						$month = substr($period,0,strpos($period,"/"));
						$year = substr($period,strpos($period,"/")+1);
					}
					return $this->histoContribMonthByDay($year, $month);
					break;
				case 'lastmonthbyday':
					$dt = getdate();
					$month = $dt['mon'];
					$year = $dt['year'];
					if($month == 1) {
						$month = 12;
						$year--;
					} else {
						$month--;
					}

					return $this->histoContribMonthByDay($year, $month);
					break;
				case 'lastyear':
					break;
			}

	}

	/**
	 * Bar graph of the number of contrib by day
	 * for the month $month of the year $year
	 */
	function histoContribMonthByDay($year, $month) {

		global $conf;

		$month = intval($month);

		$date_value = array();

		$this->getAllChanges();

		foreach($this->allChanges as $singleChange) {
			if ($singleChange['user'] != "" && $this->toBeCounted($singleChange['page'])) {
				if($singleChange['type'] != "D" && $singleChange['type'] != "R" ) {
					if(date("n/Y", $singleChange['date']) == $month."/".$year) {
						$date_value[date("j", $singleChange['date'])]++;
					}
				}
			}
		}

		// getting last day of the current month
		$monthTime = mktime(0, 0, 0, $month, 1, $year);
		$lastDayOfMonth = date("t", $monthTime);

		// Dataset definition
		$dataSet = new pData;
		$tabDays = array();
		$tabVals = array();
		for ($i = 1; $i <= $lastDayOfMonth; $i++) {
			$tabDays[$i] = $i;
			$tabVals[$i] = $date_value[$i];
		}

		/*
		$ret .= implode(',', $tabDays);
		$ret .= "<br />";
		$ret .= implode(',', $tabVals);
		$ret .= "<br />";
		*/

		$dataSet->AddPoint($tabVals, "Serie1");
		$dataSet->AddPoint($tabDays, "Serie2");
		$dataSet->AddSerie("Serie1");
		$dataSet->SetAbsciseLabelSerie("Serie2");

		// Graph width
		$width = $this->initOpt['width'];
		if($width == '') {
			$width = 700;
		}

		// Graph height
		$height = $this->initOpt['height'];
		if($height == '') {
			$height = 230;
		}

		$heightForAngle = 0;
		$absLabelAngle = $this->initOpt['absLabelAngle'];
		if($absLabelAngle > 0 && $absLabelAngle <= 90) {
			$heightForAngle = $absLabelAngle/10;
		} else {
			$absLabelAngle = 0;
		}

		// Initialize the graph
		$chart = new pChart($width,$height+$heightForAngle);
		$chart->setGraphArea(40,30,$width-20,$height-30);

		// load colour palette from currently active template if exists.
		// Otherwise fall back to default colour palette
		if (@file_exists(DOKU_TPLINC.'palette.txt'))
			$chart->loadColorPalette(DOKU_TPLINC.'palette.txt');
		else
			$chart->loadColorPalette(DOKU_PLUGIN.'wikistatistics/palette.txt');

		$chart->setFontProperties(DOKU_PLUGIN.'wikistatistics/Fonts/tahoma.ttf',10);
		$chart->drawFilledRoundedRectangle(7,7,$width-7,$height-7+$heightForAngle,5,240,240,240);
		$chart->drawRoundedRectangle(5,5,$width-5,$height-5+$heightForAngle,5,230,230,230);
		$chart->drawGraphArea(252,252,252);
		// definition of drawScale method : drawScale($Data,$DataDescription,$ScaleMode,$R,$G,$B,$DrawTicks=TRUE,$Angle=0,$Decimals=1,$WithMargin=FALSE,$SkipLabels=1,$RightScale=FALSE)
		$chart->drawScale($dataSet->GetData(),$dataSet->GetDataDescription(),SCALE_NORMAL,150,150,150,TRUE,$absLabelAngle,2,TRUE);
		$chart->drawGrid(4,TRUE,230,230,230,255);

		// Draw the bar graph
		$chart->drawBarGraph($dataSet->GetData(),$dataSet->GetDataDescription(),TRUE);

		// Finish the graph
		$chart->setFontProperties(DOKU_PLUGIN.'wikistatistics/Fonts/tahoma.ttf',10);
		$chart->drawTitle(0,0,$this->getLang('ws_histocontribmonthbydaytitle').' '.date('F', $monthTime).' '.date('Y', $monthTime),50,50,50,$width,35);

		if (!is_dir($conf['mediadir'].'/wikistatistics'))
		{
			io_mkdir_p($conf['mediadir'].'/wikistatistics'); //Using dokuwiki framework
		}
		$chart->Render($conf['mediadir']."/wikistatistics/histocontrib_$month_$year.png");

		$url = ml("wikistatistics:histocontrib_$month_$year.png"); //Using dokuwiki framework

		$ret .= '
	<img src="' . $url . '" alt="' . $this->getLang('ws_histocontribmonthbydaytitle').' '.date('F', $monthTime).' '.date('Y', $monthTime) . '" title="' . $this->getLang('ws_histocontribmonthbydaytitle').' '.date('F', $monthTime).' '.date('Y', $monthTime) . '"/>';

		return $ret;
	}

	/**
	 * Bar graph of the number of contrib by month
	 */
	function histoContribByMonth() {

		global $conf;

		$date_value = array();
		$dateMin = '';

		$this->getAllChanges();

		foreach($this->allChanges as $singleChange) {
			if ($singleChange['user'] != "" && $this->toBeCounted($singleChange['page'])) {
				if($singleChange['type'] != "D" && $singleChange['type'] != "R" ) {
					$dateContrib =	date("m/Y", $singleChange['date']);
					$date_value[$dateContrib]++;
					if($dateMin == '' || $singleChange['date'] < $dateMin) {
						$dateMin = $singleChange['date'];
					}
				}
			}
		}

		$dateMin = date("m/Y",$dateMin);

		// getting current month : end of the graph
		$monthNow = date("m/Y");

		// Dataset definition
		$dataSet = new pData;
		$tabMonths = array();
		$tabVals = array();

		$previousMonth = '';
		$currentMonth = $dateMin;
		$i = 0;
		$out = false;

		// since the month of the first contrib to now...
		while($previousMonth == '' || $previousMonth != $monthNow) {
			$tabMonths[$i] = $currentMonth;
			$tabVals[$i] = $date_value[$currentMonth];

			$previousMonth = $currentMonth;

			// calculate next month
			$m = substr($currentMonth,0,strpos($currentMonth,"/"));
			$y = substr($currentMonth,strpos($currentMonth,"/")+1);
			if($m == 12) {
				$m = 1;
				$y++;
			} else {
				$m++;
			}
			$currentMonth = ($m < 10 ? '0' : '').$m.'/'.$y;
			$i++;
		}

		$dataSet->AddPoint($tabVals, 'Serie1');
		$dataSet->AddPoint($tabMonths, 'Serie2');
		$dataSet->AddSerie('Serie1');
		$dataSet->SetAbsciseLabelSerie('Serie2');

		// Graph width
		$width = $this->initOpt['width'];
		if($width == '') {
			$width = 700;
		}

		// Graph height
		$height = $this->initOpt['height'];
		if($height == '') {
			$height = 230;
		}

		$heightForAngle = 0;
		$absLabelAngle = $this->initOpt['absLabelAngle'];
		if($absLabelAngle > 0 && $absLabelAngle <= 90) {
			$heightForAngle = $absLabelAngle/2;
		} else {
			$absLabelAngle = 0;
		}

		// Initialize the graph
		$chart = new pChart($width,$height+$heightForAngle);
		$chart->setGraphArea(40,30,$width-20,$height-30);
		
		// load colour palette from currently active template if exists.
		// Otherwise fall back to default colour palette
		if (@file_exists(DOKU_TPLINC.'palette.txt'))
			$chart->loadColorPalette(DOKU_TPLINC.'palette.txt');
		else
			$chart->loadColorPalette(DOKU_PLUGIN.'wikistatistics/palette.txt');
		
		$chart->setFontProperties(DOKU_PLUGIN.'wikistatistics/Fonts/tahoma.ttf',10);
		$chart->drawFilledRoundedRectangle(7,7,$width-7,$height-7+$heightForAngle,5,240,240,240);
		$chart->drawRoundedRectangle(5,5,$width-5,$height-5+$heightForAngle,5,230,230,230);
		$chart->drawGraphArea(252,252,252);
		$chart->drawScale($dataSet->GetData(),$dataSet->GetDataDescription(),SCALE_NORMAL,150,150,150,TRUE,$absLabelAngle,2,TRUE);
		$chart->drawGrid(4,TRUE,230,230,230,255);

		// Draw the bar graph
		$chart->drawBarGraph($dataSet->GetData(),$dataSet->GetDataDescription(),TRUE);

		// Finish the graph
		$chart->setFontProperties(DOKU_PLUGIN.'wikistatistics/Fonts/tahoma.ttf',10);
		$chart->drawTitle(0,0,$this->getLang('ws_histocontribbymonthtitle'),50,50,50,$width,35);

		if (!is_dir($conf['mediadir'] . '/wikistatistics'))
		{
			io_mkdir_p($conf['mediadir'] . '/wikistatistics'); //Using dokuwiki framework
		}
		$chart->Render($conf['mediadir'].'/wikistatistics/histocontrib_bymonth.png');

		$url = ml('wikistatistics:histocontrib_bymonth.png'); //Using dokuwiki framework

		$ret .= '
	<img src="' . $url . '" alt="' . $this->getLang('ws_histocontribbymonthtitle') . '" title="' . $this->getLang('ws_histocontribbymonthtitle') . '"/>';

		return $ret;
	}

	/*
	function countPagesExceptCont($ns='',$path='') {
		global $conf;

		if($ns == '') {
			$ns = split(":",$this->initOpt['ns']);
		}

		$nsArray = split(":",$ns);

		if ($path == '') {
			$path = $conf['datadir'];
		}

		if ($pdir = opendir($path)) {
			while ($file = readdir($pdir)) {
				if ($file != "." && $file != "..") {
					if (is_file($path."/".$file)) {
						$tmp++;
					} else if (is_dir($path."/".$file) && $file != "wiki" && $file != "talk" && $file != "playground" && @!strstr($file,$ns[0]) ) {
						$tmp += $this->countPagesExceptCont($nsArray,$path."/".$file);
					}
				}
			}
			closedir($pdir);
		}

		return $tmp;
	}
	*/

	/*
	function countPagesExcept() {
		$totalPages = $this->countAllPages();
		$exceptPages = $this->countAllPages($this->initOpt['excludens']);
		return $totalPages - $exceptPages;
	}
	*/

	/**
	 * Count the pages in the namespace $ns
	 */
	function countPages($ns) {
		global $conf;

		// root directory of all pages
		$rootPath = $conf['datadir'];

		$path = '';

		// go to namespace directory if specified
		if($ns != '') {
			$nsArray = split(":",$ns);

			foreach ($nsArray as $namespace) {
				$path .= "/".$namespace;
			}
		}

		return $this->_pages_xhtml_r($path, $rootPath);
	}

	/**
	 * Recursive method to count the number of pages under $path
	 */
	function _pages_xhtml_r($path, $rootPath) {
		$nbPages = 0;

		$nsPath = str_replace('/',':',$path);
		if($path == '' || $this->toBeCounted($nsPath)) {
			if (is_dir($rootPath.$path)) {
				if($pdir = opendir($rootPath.$path)) {
					while ($file = readdir($pdir)) {
						if ($file != "." && $file != "..") {

							$filePath = $path."/".$file;

							if (is_file($rootPath.$filePath)) {
								$filens = substr($filePath, 0, strrpos($filePath , '.txt'));

								$filens = str_replace('/',':',$filens);
								if($this->toBeCounted($filens)) {
									$nbPages++;
								}
							} else {
								$nbPages += $this->_pages_xhtml_r($filePath, $rootPath);
							}
						}
					}
					closedir($pdir);
				}
			}
		}

			return $nbPages;
		}


	/*
	*
	*	If the namespace/page must be counted return true, otherwise false
	*
	*/
	/*
	function toBeCounted($ns, $type = 'ns') {
//On-going
		switch ($type) {
			case 'ns': //namespace, pattern or entire
				$ns = (preg_match("/^:/",$ns)) ? substr($ns,1) : $ns;

				$ns_split = split(":",$ns);
				foreach($ns_split as $ns){
					if (in_array($ns,$this->excludedNs)) {
						return false;
					}
				}
				if(@preg_match($this->excludedns_pattern,$ns_split[0])){
					return false;
				}
				break;
			case 'pg': //page, only pattern
				break;
		}

		return true;
	}
	*/

	/**
	 * Check if the namespace $ns has to be excluded or not
	 */
	function toBeCounted($ns) {

		//namespace
		$ns = (preg_match("/^:/",$ns)) ? substr($ns,1) : $ns;

		$nstocheck = "";

		$ns_split = split(":",$ns);
		foreach($ns_split as $nspart){
			if($nstocheck != "") {
				$nstocheck .= ":";
			}
			$nstocheck .= $nspart;
			if (($this->excludedNs) != "" && in_array($nstocheck,$this->excludedNs)) {
				return false;
			}
		}

		// pattern
		if(@preg_match($this->excludedNsPattern,$ns)){
			return false;
		}

		return true;
	}

	function cw_array_count($a) {
		if(!is_array($a)) return $a;
		foreach($a as $key=>$value)
			$totale += $this->cw_array_count($value);
		return $totale;
	}


	/**
	 * Count the users
	 */
		function countUsers() {
		global $auth;

		$nbUsers = 0;

		if($this->initOpt['filter'] == 'active') {
			// only active users (those who contributed at least once)

			$users = array();
			$this->getAllChanges();

			foreach($this->allChanges as $singleChange) {
				$user = $singleChange['user'];
				if ($user != "" && !in_array($user, $users)) {
					$users[] = $user;
				}
			}
			$nbUsers = sizeof($users);
		} else {
			// all users

			// if the auth module implements the getUserCount function, use it !
			// it's not the case for ldap auth for example.
			if($auth->canDo('getUserCount')) {
				$nbUsers = $auth->getUserCount(array());
			}
		}

		return $nbUsers;
	}


	function getAllChanges($directory='',$first=0,$num=0,$ns='',$flags=0) {
		global $conf;
		if(@file_exists($conf['mediadir'].'/wikistatistics/cache_changes.php')){
			include($conf['mediadir'].'/wikistatistics/cache_changes.php');
		}

		if(time() > $this->lastUpdate + $this->getConf('ws_cacheexpire')){
			@unlink($conf['mediadir'].'/wikistatistics/cache_changes.php');
			unset($this->allChanges);
			$i=0;

			$this->parseChanges($directory,$first,$num,$ns,$flags);

			$fp = @fopen( $conf['mediadir'].'/wikistatistics/cache_changes.php', 'w' );
			@fwrite( $fp, '<?php
		$this->lastUpdate = ' . time() . ';');
			foreach ($this->allChanges as $change){
				@fwrite( $fp, '
		$this->allChanges['.$i.'] = array(
			\'date\' => ' . $change['date'] . ',
			\'ip\' => \'' . $change['ip'] . '\',
			\'type\' => \'' . $change['type'] . '\',
			\'id\' => \'' . $change['id'] . '\',
			\'user\' => \'' . $change['user'] . '\',
			\'sum\' => \'' . $change['sum'] . '\',
			\'extra\' => \'' . $change['extra'] . '\',
			);');
				$i++;
			}
			@fclose( $fp );
			include($conf['mediadir'].'/wikistatistics/cache_changes.php');
		}
	}
	
	function parseChanges($directory='',$first=0,$num=0,$ns='',$flags=0) {
		global $conf;
		$metapath = $conf['metadir'];
		$count = 0;
		if(empty($directory)) {
			$directory=dirname($conf['changelog']);
		}

		//Counting files variable initialization
		$count=0;

		/*
		* open the directory and take an instance of it to handle var
		*/
		if ($handle = opendir($directory)) {
			$sub = substr($directory,strpos($directory,$metapath)+strlen($metapath)+1);
			$sub = str_replace('/',':',$sub);
			if($this->toBeCounted($sub,'ns')) {
				while (false !== ($file = readdir($handle))) {

					if ($file != "." && $file != "..") {
						if (is_file($directory."/".$file)) {
							// Determining extensions of files
							$file_ext = substr($file, strrpos($file, ".")+1);

							if($file_ext == 'changes' && $file != '_dokuwiki.changes') {
								$lines = @file("$directory/$file");
								for($i = count($lines)-1; $i >= 0; $i--) {
									$rec = parseChangelogLine($lines[$i]);
									if($rec !== false) {
										if(--$first >= 0) continue; // skip first entries
										$this->allChanges[] = $rec;
										$count++;
										// break when we have enough entries
										if(!$num==0) {
											if($count >= $num) {
												break;
											}
										}
									}
								}
							}
						} else if (is_dir("$directory/$file")) {
							$this->parseChanges("$directory/$file");
						}
					}
				}
			}

			closedir($handle);
		}
	}

	function pagesSizes($ns = '') {
		global $conf;

		// root directory of all pages
		$rootPath = $conf['datadir'];

		$path = '';

		// go to namespace directory if specified
		if($ns != '') {
			$nsArray = split(':',$ns);

			foreach ($nsArray as $namespace) {
				$path .= '/'.$namespace;
			}
		}

		// Max level
		$depthLevel = $this->initOpt['depthlevel'];
		if($depthLevel == '') {
			$depthLevel = 0;
		}

		$pagesSizes = $this->getAllPagesSizes($path, $rootPath, $depthLevel);

		$nss = array();
		$sizes = array();
		foreach($pagesSizes as $ns => $size) {
			$nss[] = $ns." (".$size.")";
			$sizes[] = $size;
		}

		// Graph width
		$width = $this->initOpt['width'];
		if($width == '') {
			$width = 530;
		}

		// Graph height
		$height = $this->initOpt['height'];
		if($height == '') {
			$height = 200;
		}

		$legendX = $width-min(220, round($width/3));
		$graphX = round($legendX/2);
		$graphY = round($height/2);
		$graphR = round(($graphX<$graphY)?$graphX:min($graphX,$graphY*1.6))-50;

		// Dataset definition
		$DataSet = new pData;
		$DataSet->AddPoint($sizes,'sizes');
		$DataSet->AddPoint($nss,'namespaces');
		$DataSet->AddAllSeries();
		$DataSet->SetAbsciseLabelSerie('namespaces');

		// Initialise the graph
		$chart = new pChart($width,$height);
		$chart->drawFilledRoundedRectangle(7,7,$width-7,$height-7,5,240,240,240);
		$chart->drawRoundedRectangle(5,5,$width-5,$height-5,5,230,230,230);

		// load colour palette from currently active template if exists.
		// Otherwise fall back to default colour palette
		if (@file_exists(DOKU_TPLINC.'palette.txt'))
			$chart->loadColorPalette(DOKU_TPLINC.'palette.txt');
		else
			$chart->loadColorPalette(DOKU_PLUGIN.'wikistatistics/palette.txt');

		// Draw the pie chart
		$chart->setFontProperties(DOKU_PLUGIN.'wikistatistics/Fonts/tahoma.ttf',10);
		// drawPieGraph($Data,$DataDescription,$XPos,$YPos,$Radius=100,$DrawLabels=PIE_NOLABEL,$EnhanceColors=TRUE,$Skew=60,$SpliceHeight=20,$SpliceDistance=0,$Decimals=0)
		$chart->drawPieGraph($DataSet->GetData(),$DataSet->GetDataDescription(),$graphX,$graphY,$graphR,PIE_PERCENTAGE,TRUE,50,20,5);
		$chart->drawPieLegend($legendX,15,$DataSet->GetData(),$DataSet->GetDataDescription(),250,250,250);

		$chart->Render($conf['mediadir'].'/wikistatistics/pagessizes.png');

		$url = ml('wikistatistics:pagessizes.png'); //Using dokuwiki framework

		$ret = '
	<img src="' . $url . '" alt="' . $this->getLang('ws_pagesize') . '" title="' . $this->getLang('ws_pagesize') . '"/>';

		return $ret;

	}

	/**
	 * Calculate the size of all pages
	 */
	function getAllPagesSizes($path, $rootPath, $depthLevel) {
		$pagesSizes = array();

		$nsPath = str_replace('/',':',$path);
		if($path == '' || $this->toBeCounted($nsPath)) {
			if (is_dir($rootPath.$path)) {
				if($pdir = opendir($rootPath.$path)) {
					while ($file = readdir($pdir)) {
						if ($file != '.' && $file != '..') {

							$filePath = "$path/$file";

							if (is_file($rootPath.$filePath)) {
								$filens = substr($filePath, 0, strrpos($filePath , '.txt'));

								$filens = str_replace('/',':',$filens);
								if(substr($filens, 0, 1) == ':') {
									$filens = substr($filens, 1);
								}
								if($this->toBeCounted($filens)) {
									if($depthLevel > 0) {
										$nsArray = split(':', $filens);
										$filens = '';
										for($i=0; $i<sizeof($nsArray) && $i<$depthLevel; $i++) {
											if($i > 0) {
												$filens .= ':';
											}
											$filens .= $nsArray[$i];

										}
									}

									//echo $filePath." -> ".$filens." -> ";
									$pagesSizes[$filens] = filesize($rootPath.$filePath);
									//echo $pagesSizes[$filens]."<br />";
								}
							} else {
							//if (is_dir($rootPath.$filePath)) {
							//if (true) {
								foreach($this->getAllPagesSizes($filePath, $rootPath, $depthLevel) as $ns => $size) {
									$pagesSizes[$ns] += $size;
								}
							}
						}
					}
					closedir($pdir);
				}
			}
		}

		array_multisort($pagesSizes, SORT_DESC);
		
		return $pagesSizes;
	}

	function getTopPagesSizes($path = '') {
		global $conf;

		// root directory of all pages
		$rootPath = $conf['datadir'];

		//$path = '';

		// nb of rows to display
		// if missing, 10 will be taken as default value
		$nbOfRows = (isset($this->initOpt['nbOfRows']) && is_numeric($this->initOpt['nbOfRows'])) ? $this->initOpt['nbOfRows'] : 10;

		$pagesSizes = $this->getAllPagesSizes($path, $rootPath, 0);

		arsort($pagesSizes);

		$ret = '
	<table class="info_hof inline">
		<caption class="hof_caption">'.$this->getLang('ws_hofpagessizes').'</caption>
		<tr>
			<th class="centeralign">'.$this->getLang('ws_position').'</th>
			<th class="centeralign">'.$this->getLang('ws_page').'</th>
			<th class="centeralign">'.$this->getLang('ws_size').'</th>
		</tr>';

		$i = 0;
		foreach($pagesSizes as $page => $pagesize) {
			$evenodd = $i++ % 2 ? 'hof_evenrow' : 'hof_oddrow';
			if ($nbOfRows == '-1' || $i <= $nbOfRows) {
				$ret .= '
		<tr class="'.$evenodd.'">
			<td class="hof_row_pos"><b>'.$i.'</b></td>
			<td class="hof_row_name">'.html_wikilink(':'.$page).'</td>
			<td class="hof_row_num">'.$pagesize.'</td>
		</tr>';
			}
		}

		$ret .= '
	</table>';

		return $ret;
	}

	/*
	*
	*	Variable's initialization
	*
	*/
	function varinit($params) {
		$this->allChanges = array(); //Reset the counter

		$this->initOpt=$params;

		// param ws_excludedns
		$this->excludedNs = split(",",$this->getConf('ws_excludedns'));

		// param ws_excludedns_pattern
		$excludedns_pattern = split(",",$this->getConf('ws_excludedns_pattern'));
		$this->excludedNsPattern = '/';
		for($i=0;$i<count($excludedns_pattern);$i++){
			if($excludedns_pattern[$i]!='')
				$this->excludedNsPattern .= $excludedns_pattern[$i].'|';
		}
		$this->excludedNsPattern = substr($this->excludedNsPattern, 0, -1).'/';
		if($this->excludedNsPattern=='/'){$this->excludedNsPattern='';}

		//page patterns
		//$this->excludedpg_pattern = split(",",$this->getConf('ws_excludedpg_pattern'));
	}
}
