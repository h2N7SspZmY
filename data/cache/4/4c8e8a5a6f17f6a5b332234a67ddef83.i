a:120:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1;}i:3;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:-1;i:1;i:0;i:2;i:1;i:3;s:0:"";}i:2;i:1;}i:4;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:3:"Git";i:1;i:1;i:2;i:1;}i:2;i:1;}i:5;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:6;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:18;}i:7;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:"
";}i:2;i:19;}i:8;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:19:"http://git-scm.com/";i:1;s:3:"Git";}i:2;i:20;}i:9;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:": ";}i:2;i:47;}i:10;a:3:{i:0;s:13:"interwikilink";i:1;a:4:{i:0;s:17:"wp>Git_(software)";i:1;s:3:"Git";i:2;s:2:"wp";i:3;s:14:"Git_(software)";}i:2;i:49;}i:11;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:74;}i:12;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:75;}i:13;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:77;}i:14;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:15;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:79;}i:16;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:1;i:1;i:78;i:2;i:1;i:3;s:3:"Git";}i:2;i:79;}i:17;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:18:"How to install Git";i:1;i:2;i:2;i:79;}i:2;i:79;}i:18;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:79;}i:19;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:135:"
$ sudo apt-get install git-core
$ git config --global user.name user_name
$ git config --global user.email you@yourdomain.example.com
";i:1;s:4:"bash";i:2;N;}i:2;i:115;}i:20;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:21;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:265;}i:22;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:79;i:1;i:264;i:2;i:2;i:3;s:18:"How to install Git";}i:2;i:265;}i:23;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:23:"How to create a SSH key";i:1;i:2;i:2;i:265;}i:2;i:265;}i:24;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:265;}i:25;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:14:"
$ ssh-keygen
";i:1;s:4:"bash";i:2;N;}i:2;i:307;}i:26;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:27;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:337;}i:28;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:265;i:1;i:336;i:2;i:2;i:3;s:23:"How to create a SSH key";}i:2;i:337;}i:29;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:23:"How to create a project";i:1;i:2;i:2;i:337;}i:2;i:337;}i:30;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:337;}i:31;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:372;}i:32;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"
Using ";}i:2;i:373;}i:33;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:17:"http://repo.or.cz";i:1;s:23:"Repo Public Git Hosting";}i:2;i:380;}i:34;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:425;}i:35;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:173:"
$ cd path/to/project
$ git init
$ git add .
$ git commit -a -m"message"
$ git remote add origin ssh://user_name@repo.or.cz/srv/git/project_name.git
$ git push --all origin
";i:1;s:4:"bash";i:2;N;}i:2;i:432;}i:36;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:618;}i:37;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:6:"Using ";}i:2;i:620;}i:38;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:19:"https://github.com/";i:1;s:6:"GitHub";}i:2;i:626;}i:39;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:656;}i:40;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:205:"
$ mkdir project_name
$ cd project_name
$ git init
$ touch README
$ git add README
$ git commit -m 'first commit'
$ git remote add origin git@github.com:user_name/project_name.git
$ git push origin master
";i:1;s:4:"bash";i:2;N;}i:2;i:663;}i:41;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:42;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:883;}i:43;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:337;i:1;i:882;i:2;i:2;i:3;s:23:"How to create a project";}i:2;i:883;}i:44;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:25:"How to download a project";i:1;i:2;i:2;i:883;}i:2;i:883;}i:45;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:883;}i:46;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:920;}i:47;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"
Using ";}i:2;i:921;}i:48;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:17:"http://repo.or.cz";i:1;s:23:"Repo Public Git Hosting";}i:2;i:928;}i:49;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:973;}i:50;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:65:"
$ git clone git://repo.or.cz/project_name.git
$ cd project_name
";i:1;s:4:"bash";i:2;N;}i:2;i:980;}i:51;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1058;}i:52;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:6:"Using ";}i:2;i:1060;}i:53;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:19:"https://github.com/";i:1;s:6:"GitHub";}i:2;i:1066;}i:54;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1096;}i:55;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:76:"
$ git clone http://github.com/user_name/project_name.git
$ cd project_name
";i:1;s:4:"bash";i:2;N;}i:2;i:1103;}i:56;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:57;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1194;}i:58;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:883;i:1;i:1193;i:2;i:2;i:3;s:25:"How to download a project";}i:2;i:1194;}i:59;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:23:"How to upload a project";i:1;i:2;i:2;i:1194;}i:2;i:1194;}i:60;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:1194;}i:61;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1229;}i:62;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:21:"
Just the first time:";}i:2;i:1230;}i:63;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1251;}i:64;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1251;}i:65;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:6:"Using ";}i:2;i:1253;}i:66;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:17:"http://repo.or.cz";i:1;s:23:"Repo Public Git Hosting";}i:2;i:1259;}i:67;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1304;}i:68;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:100:"
$ git remote rm origin
$ git remote add origin ssh://user_name@repo.or.cz/srv/git/project_name.git
";i:1;s:4:"bash";i:2;N;}i:2;i:1311;}i:69;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1424;}i:70;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:6:"Using ";}i:2;i:1426;}i:71;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:19:"https://github.com/";i:1;s:6:"GitHub";}i:2;i:1432;}i:72;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1462;}i:73;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:90:"
$ git remote rm origin
$ git remote add origin git@github.com:user_name/project_name.git
";i:1;s:4:"bash";i:2;N;}i:2;i:1469;}i:74;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1572;}i:75;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:8:"Always:
";}i:2;i:1574;}i:76;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1587;}i:77;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:90:"
$ git add .
$ git commit -a -m"description of the modifications"
$ git push --all origin
";i:1;s:4:"bash";i:2;N;}i:2;i:1587;}i:78;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:79;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1692;}i:80;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:1194;i:1;i:1691;i:2;i:2;i:3;s:23:"How to upload a project";}i:2;i:1692;}i:81;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:39:"How to update your version of a project";i:1;i:2;i:2;i:1692;}i:2;i:1692;}i:82;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:1692;}i:83;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:26:"
$ git pull origin master
";i:1;s:4:"bash";i:2;N;}i:2;i:1750;}i:84;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:85;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1793;}i:86;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:1692;i:1;i:1792;i:2;i:2;i:3;s:39:"How to update your version of a project";}i:2;i:1793;}i:87;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:19:"How to create a tag";i:1;i:2;i:2;i:1793;}i:2;i:1793;}i:88;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:1793;}i:89;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:20:"
$ git tag tag_name
";i:1;s:4:"bash";i:2;N;}i:2;i:1831;}i:90;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:91;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1866;}i:92;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:1793;i:1;i:1865;i:2;i:2;i:3;s:19:"How to create a tag";}i:2;i:1866;}i:93;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:19:"How to upload a tag";i:1;i:2;i:2;i:1866;}i:2;i:1866;}i:94;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:1866;}i:95;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:28:"
$ git push origin tag_name
";i:1;s:4:"bash";i:2;N;}i:2;i:1904;}i:96;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1945;}i:97;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:"or";}i:2;i:1947;}i:98;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1949;}i:99;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:26:"
$ git push --tags origin
";i:1;s:4:"bash";i:2;N;}i:2;i:1956;}i:100;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:101;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1997;}i:102;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:1866;i:1;i:1996;i:2;i:2;i:3;s:19:"How to upload a tag";}i:2;i:1997;}i:103;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:15:"Git Cheat Sheet";i:1;i:2;i:2;i:1997;}i:2;i:1997;}i:104;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:1997;}i:105;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2024;}i:106;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:"
";}i:2;i:2025;}i:107;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:95:"http://cheat.errtheblog.com/s/git?utm_source=twitter&utm_medium=micro-blog&utm_campaign=twitter";i:1;s:9:"Cheat Git";}i:2;i:2026;}i:108;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2135;}i:109;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:110;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:2137;}i:111;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:1997;i:1;i:2136;i:2;i:2;i:3;s:15:"Git Cheat Sheet";}i:2;i:2137;}i:112;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:20:"Resumo em português";i:1;i:2;i:2;i:2137;}i:2;i:2137;}i:113;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:2137;}i:114;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:1008:"
Tens que te cadastrar no github e me dizer teu usuário para poderes dar pull

http://github.com/kssilveira/CTipsAndTricks

chave ssh:
cat ~/.ssh/id_rsa.pub
caso o arquivo exista, coloca o conteúdo dele na tua lista de chaves ssh no teu perfil do git hub
caso não exista:
   ssh-keygen
   {enter}
   escolhe uma senha
   goto chave ssh

git config --global user.name "Bruno Jurkovski"
git config --global user.email bjurkovski@gmail.com
git clone git@github.com:kssilveira/CTipsAndTricks.git
... edita ...
git status
... make clean nas pastas com os slides ... apagar *~, *.o, executáveis e etc. de todas as pastas ...
git status
... ver se tem apenas arquivos que não são derivados (além dos pdfs) ...
git add .
git status
... verificar novamente ...
git commit -a -m"descrição"
... isso cria uma versão local (só na tua máquina), que funciona da mesma forma que se tivesse no servidor ...
git push --all origin
... upload para o servidor ...
git pull origin master
... download do servidor ...
";i:1;s:4:"bash";i:2;N;}i:2;i:2175;}i:115;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"tag_tag";i:1;a:1:{i:0;s:11:"programming";}i:2;i:5;i:3;s:19:"{{tag>programming}}";}i:2;i:3198;}i:116;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:117;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:3217;}i:118;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:2137;i:1;i:0;i:2;i:2;i:3;s:20:"Resumo em português";}i:2;i:3217;}i:119;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:3217;}}