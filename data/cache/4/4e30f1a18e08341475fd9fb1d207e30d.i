a:24:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1;}i:3;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:-1;i:1;i:0;i:2;i:1;i:3;s:0:"";}i:2;i:1;}i:4;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:2:"Go";i:1;i:1;i:2;i:1;}i:2;i:1;}i:5;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:6;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:17;}i:7;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:"
";}i:2;i:18;}i:8;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:18:"http://golang.org/";i:1;s:2:"Go";}i:2;i:19;}i:9;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:": ";}i:2;i:44;}i:10;a:3:{i:0;s:13:"interwikilink";i:1;a:4:{i:0;s:28:"wp>Go_(programming_language)";i:1;s:2:"Go";i:2;s:2:"wp";i:3;s:25:"Go_(programming_language)";}i:2;i:46;}i:11;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:81;}i:12;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:13;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:83;}i:14;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:1;i:1;i:82;i:2;i:1;i:3;s:2:"Go";}i:2;i:83;}i:15;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:8:"Examples";i:1;i:2;i:2;i:83;}i:2;i:83;}i:16;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:83;}i:17;a:3:{i:0;s:4:"file";i:1;a:3:{i:0;s:71:"
PROG = prog_name

all: $(PROG)

%: %.8
	8l -o $@ $<

%.8: %.go
	8g $<
";i:1;s:4:"make";i:2;s:8:"makefile";}i:2;i:110;}i:18;a:3:{i:0;s:4:"file";i:1;a:3:{i:0;s:286:"
/* hello */

package main // executável principal sempre deve pertencer a este pacote

import . "fmt" // importa funções de saída formatada (Printf)

// função principal (como em C)
func main() {
	//sem o . no import teríamos que utilizar fmt.Printf
	Printf("hello, world\n")
}
";i:1;s:1:"c";i:2;s:8:"hello.go";}i:2;i:210;}i:19;a:3:{i:0;s:4:"file";i:1;a:3:{i:0;s:2013:"
/* pc_channel_multi.go */

package main

import (
	. "fmt"
	"crypto/rand"
	"time"
	"flag"
)

func random_between (begin, end int64) int64 {
	random_byte := make([]byte, 1)
	rand.Read(random_byte)
	return int64(random_byte[0]) % (end - begin + 1) + begin
}

func random_sleep () {
	time.Sleep(random_between(0, *MAX_SLEEP_TIME) * 10e7)
}

func send (channel chan int, i int) {
	channel <- i
}

func recv (channel chan int) int {
	return <-channel
}

func signal (channel chan int) {
	if len(channel) < cap(channel) || cap(channel) == 0 {
		channel <- 0
	}
}

func wait (channel chan int) {
	<-channel
}

var (
	BUFFER_SIZE = flag.Int("s", 5, "BUFFER_SIZE") // flag -s
	MAX_SLEEP_TIME = flag.Int64("t", 5, "MAX_SLEEP_TIME") // flag -t
	MAX_PRODUCTIONS = flag.Int("m", 0, "MAX_PRODUCTIONS, 0 for infinity") // flag -m
	N_PRODUCERS = flag.Int("p", 3, "N_PRODUCERS") // flag -p
	N_CONSUMERS = flag.Int("c", 3, "N_CONSUMERS") // flag -c
	pipe = make(chan int, *BUFFER_SIZE)
	quit 	= make(chan int)
)

func producer (id int) {
	for i := 0; i < *MAX_PRODUCTIONS || *MAX_PRODUCTIONS == 0; i++ {
		Println(id, "producing:", i + id * 1e5)
		send(pipe, i + id * 1e5)
		random_sleep()
	}
	signal(quit)
}

func consumer (id int) {
	for {
		v := recv(pipe)
		if closed(pipe) { // canal fechado?
			break
		}
		Println("\t\t\t", id, "consuming:", v)
		random_sleep()
	}
	signal(quit)
}

func main () {
	flag.Parse() // processa flags
	Println("BUFFER_SIZE =", *BUFFER_SIZE)
	Println("MAX_SLEEP_TIME =", *MAX_SLEEP_TIME)
	Println("MAX_PRODUCTIONS =", *MAX_PRODUCTIONS)
	Println("N_PRODUCERS =", *N_PRODUCERS)
	Println("N_CONSUMERS =", *N_CONSUMERS)
	for i := 0; i < *N_PRODUCERS; i++ { // cria produtores
		go producer(i + 1)
	}
	for i := 0; i < *N_CONSUMERS; i++ { // cria consumidores
		go consumer(i + 1)
	}
	for i := 0; i < *N_PRODUCERS; i++ { // espera o término dos produtores
		wait(quit)
	}
	close(pipe)	// fecha comunicação
	for i := 0; i < *N_CONSUMERS; i++ {	// espera o término dos consumidores
		wait(quit)
	}
}
";i:1;s:1:"c";i:2;s:20:"producer_consumer.go";}i:2;i:522;}i:20;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:21;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:2566;}i:22;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:83;i:1;i:0;i:2;i:2;i:3;s:8:"Examples";}i:2;i:2566;}i:23;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:2566;}}