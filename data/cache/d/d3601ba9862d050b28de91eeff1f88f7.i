a:94:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1;}i:3;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:-1;i:1;i:0;i:2;i:1;i:3;s:0:"";}i:2;i:1;}i:4;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:5:"LaTeX";i:1;i:1;i:2;i:1;}i:2;i:1;}i:5;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:6;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:20;}i:7;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:"
";}i:2;i:21;}i:8;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:29:"http://www.latex-project.org/";i:1;s:5:"LaTeX";}i:2;i:22;}i:9;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:": ";}i:2;i:61;}i:10;a:3:{i:0;s:13:"interwikilink";i:1;a:4:{i:0;s:8:"wp>LaTeX";i:1;s:5:"LaTeX";i:2;s:2:"wp";i:3;s:5:"LaTeX";}i:2;i:63;}i:11;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:81;}i:12;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:13;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:83;}i:14;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:1;i:1;i:82;i:2;i:1;i:3;s:5:"LaTeX";}i:2;i:83;}i:15;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:25:"How use BibTeX with LaTeX";i:1;i:2;i:2;i:83;}i:2;i:83;}i:16;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:83;}i:17;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:120;}i:18;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"
See ";}i:2;i:121;}i:19;a:3:{i:0;s:12:"internallink";i:1;a:2:{i:0;s:11:":tools:make";i:1;N;}i:2;i:126;}i:20;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:6:". See ";}i:2;i:141;}i:21;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:55:"http://www.kfunigraz.ac.at/~binder/texhelp/bibtx-7.html";i:1;s:26:"Help on BibTeX Entry types";}i:2;i:147;}i:22;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:".";}i:2;i:233;}i:23;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:234;}i:24;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:25;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:236;}i:26;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:83;i:1;i:235;i:2;i:2;i:3;s:25:"How use BibTeX with LaTeX";}i:2;i:236;}i:27;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:59:"How to put utf8 characters on listings environment in LaTeX";i:1;i:2;i:2;i:236;}i:2;i:236;}i:28;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:236;}i:29;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:979:"
\lstset{extendedchars=true,
	inputencoding=utf8,
	basicstyle=\small\color{blue},
	keywordstyle=\color{red}\bfseries,
	keywordstyle= [2]\color{magenta}\bfseries,
	identifierstyle=\color{blue},
	commentstyle=\color{gray}\textit,
	stringstyle=\color{darkGreen},
	showstringspaces=false,
	tabsize=2
}

\lstset{
	numbers=left,         % Números na margem da esquerda
	numberstyle=\tiny\color{darkGreen},    % Números pequenos
	stepnumber=1,         % De 1 em 1
	numbersep=5pt         % Separados por 5pt
}

\begin{lstlisting}[language=Shell, texcl=true, numbers=none, escapechar={\%}]
\end{lstlisting}

\lstinputlisting[firstline=11, lastline=17, language=Go, texcl=true, numbers=none, escapechar={\$}]{../pc_channel.go}
\lstinputlisting[firstline=18, lastline=22, language=Go, texcl=true, numbers=none, escapechar={\#}]{../pc_channel.go}

\begin{lstlisting}[language=Shell, texcl=true, numbers=none, breaklines, escapechar={\%}]
\end{lstlisting}

\lstinline[language=Go]|chan T|

";i:1;s:5:"latex";i:2;N;}i:2;i:314;}i:30;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:31;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1309;}i:32;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:236;i:1;i:1308;i:2;i:2;i:3;s:59:"How to put utf8 characters on listings environment in LaTeX";}i:2;i:1309;}i:33;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:58:"How to make a multicolumn / multirow tabular cell in LaTeX";i:1;i:2;i:2;i:1309;}i:2;i:1309;}i:34;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:1309;}i:35;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:161:"
\usepackage{multirow}
\begin{tabular}{|c|c|}
\hline
\multicolumn{2}{|c|}{Two Column} \\
\hline
\multirow{2}{*}{Two Row} & one \\
 & two \\
\hline
\end{tabular}
";i:1;s:5:"latex";i:2;N;}i:2;i:1386;}i:36;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1561;}i:37;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"Outputs:
";}i:2;i:1563;}i:38;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1572;}i:39;a:3:{i:0;s:10:"table_open";i:1;a:2:{i:0;i:2;i:1;i:3;}i:2;i:1572;}i:40;a:3:{i:0;s:13:"tablerow_open";i:1;a:0:{}i:2;i:1572;}i:41;a:3:{i:0;s:14:"tablecell_open";i:1;a:3:{i:0;i:2;i:1;s:6:"center";i:2;i:1;}i:2;i:1572;}i:42;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:14:"  Two Column  ";}i:2;i:1574;}i:43;a:3:{i:0;s:15:"tablecell_close";i:1;a:0:{}i:2;i:1588;}i:44;a:3:{i:0;s:14:"tablerow_close";i:1;a:0:{}i:2;i:1590;}i:45;a:3:{i:0;s:13:"tablerow_open";i:1;a:0:{}i:2;i:1590;}i:46;a:3:{i:0;s:14:"tablecell_open";i:1;a:3:{i:0;i:1;i:1;s:6:"center";i:2;i:2;}i:2;i:1590;}i:47;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:11:"  Two Row  ";}i:2;i:1592;}i:48;a:3:{i:0;s:15:"tablecell_close";i:1;a:0:{}i:2;i:1603;}i:49;a:3:{i:0;s:14:"tablecell_open";i:1;a:3:{i:0;i:1;i:1;s:6:"center";i:2;i:1;}i:2;i:1603;}i:50;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"  one  ";}i:2;i:1604;}i:51;a:3:{i:0;s:15:"tablecell_close";i:1;a:0:{}i:2;i:1611;}i:52;a:3:{i:0;s:14:"tablerow_close";i:1;a:0:{}i:2;i:1612;}i:53;a:3:{i:0;s:13:"tablerow_open";i:1;a:0:{}i:2;i:1612;}i:54;a:3:{i:0;s:14:"tablecell_open";i:1;a:3:{i:0;i:1;i:1;s:6:"center";i:2;i:1;}i:2;i:1619;}i:55;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"  two  ";}i:2;i:1620;}i:56;a:3:{i:0;s:15:"tablecell_close";i:1;a:0:{}i:2;i:1627;}i:57;a:3:{i:0;s:14:"tablerow_close";i:1;a:0:{}i:2;i:1628;}i:58;a:3:{i:0;s:11:"table_close";i:1;a:0:{}i:2;i:1628;}i:59;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:60;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1630;}i:61;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:1309;i:1;i:1629;i:2;i:2;i:3;s:58:"How to make a multicolumn / multirow tabular cell in LaTeX";}i:2;i:1630;}i:62;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:54:"How to fix "LaTeX Error: Too many unprocessed floats."";i:1;i:2;i:2;i:1630;}i:2;i:1630;}i:63;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:1630;}i:64;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:14:"
  \clearpage
";i:1;s:5:"latex";i:2;N;}i:2;i:1703;}i:65;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:66;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1735;}i:67;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:1630;i:1;i:1734;i:2;i:2;i:3;s:54:"How to fix "LaTeX Error: Too many unprocessed floats."";}i:2;i:1735;}i:68;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:26:"How to use AbnTeX in LaTeX";i:1;i:2;i:2;i:1735;}i:2;i:1735;}i:69;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:1735;}i:70;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:1134:"

\documentclass[a4paper,10pt]{abnt} %
\usepackage[utf8x]{inputenc}
\usepackage[brazil]{babel}
\usepackage{graphicx}
\usepackage{listings}
\usepackage[usenames,dvipsnames]{color}
\usepackage{verbatim, amssymb, latexsym, amsmath, mathrsfs}
\usepackage{url}
\usepackage{subfigure}
\usepackage{multicol}
\usepackage{framed}
\usepackage{array}
\usepackage{float}
\usepackage[section] {placeins}
\usepackage[table]{xcolor}

%opening
%\title{}
%\author{Kauê Silveira \\171671 \and Silveira Kauê \\ 671171}

\autor{Bruno Coswig Fiss \\ Felipe Augusto Chies \\ Kauê Soares da Silveira \\ Marcos Vinicius Cavinato} %\and
\instituicao{Universidade Federal do Rio Grande do Sul}
\orientador[Professor:]{Sérgio Felipe Zirbes}
\titulo{Especificação de um Sistema de Integração On-line Empresa / Representações para Pilaplast Ind. e Com. de Plásticos Ltda.}
\comentario{Disciplina INF01127 – Engenharia de Software N}
\data{1 de julho de 2010}

\begin{document}

%\maketitle
\folhaderosto
\sumario
\listadetabelas
\listadefiguras

%\begin{abstract}
%\begin{resumo}
%\end{abstract}
%\end{resumo}

\chapter{Introdução}

\end{document}
";i:1;s:5:"latex";i:2;N;}i:2;i:1780;}i:71;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:72;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:2930;}i:73;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:1735;i:1;i:2929;i:2;i:2;i:3;s:26:"How to use AbnTeX in LaTeX";}i:2;i:2930;}i:74;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:42:"How to define a macro (a command) in LaTeX";i:1;i:2;i:2;i:2930;}i:2;i:2930;}i:75;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:2930;}i:76;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:433:"
\newcommand{\bll}{\begin{lstlisting}}
\newcommand{\bx}[1]{\begin{#1}}
\newcommand{\bfr}{\begin{frame}}
\newcommand{\fr}[2]{\begin{frame}{#1}#2\end{frame}}
\newcommand{\bl}[2]{\begin{block}{#1}#2\end{block}}
\newcommand{\bb}[1]{\begin{block}{#1}}
\newcommand{\eb}{\end{block}}
\newcommand{\bi}{\begin{itemize}}
\newcommand{\ei}{\end{itemize}}
\newcommand{\ii}{\item}
\newcommand{\bc}{\begin{comment}}
\newcommand{\ec}{\end{comment}}
";i:1;s:5:"latex";i:2;N;}i:2;i:2991;}i:77;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:78;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:3440;}i:79;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:2930;i:1;i:3439;i:2;i:2;i:3;s:42:"How to define a macro (a command) in LaTeX";}i:2;i:3440;}i:80;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:39:"How to make a table of figures in LaTeX";i:1;i:2;i:2;i:3440;}i:2;i:3440;}i:81;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:3440;}i:82;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:345:"
\begin{figure}[ht]
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=50mm]{gui0.png}
		\caption{default}
		\label{fig:figure1}
	\end{minipage}
	\hspace{0.5cm}
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=50mm]{gui1.png}
		\caption{default}
		\label{fig:figure2}
	\end{minipage}
\end{figure}
";i:1;s:5:"latex";i:2;N;}i:2;i:3498;}i:83;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:84;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:3859;}i:85;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:3440;i:1;i:3858;i:2;i:2;i:3;s:39:"How to make a table of figures in LaTeX";}i:2;i:3859;}i:86;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:29:"How to make appendix in LaTeX";i:1;i:2;i:2;i:3859;}i:2;i:3859;}i:87;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:3859;}i:88;a:3:{i:0;s:4:"file";i:1;a:3:{i:0;s:361:"
\documentclass[a4paper,10pt]{report}
\usepackage{amssymb} %for math symbols in appendix.tex
\begin{document}
\tableofcontents
\chapter{One}
\appendix
%\addcontentsline{toc}{chapter}{Appendix}
\addtocontents{toc}{\protect\contentsline{chapter}{Appendix:}{}}
\chapter{Questionnaire Requirement}
\chapter{Questionnaire Testing}
\input appendix.tex
\end{document}
";i:1;s:5:"latex";i:2;s:8:"main.tex";}i:2;i:3907;}i:89;a:3:{i:0;s:4:"file";i:1;a:3:{i:0;s:1386:"
\section{Math font styles}			% file "appendix.tex"

Several extra font styles are available in math mode,
since there are so many typographical conventions
in various branches of mathematics.\footnote{By the way,
this is an appendix!}

In set theory (among other fields) one often uses
\emph{calligraphic} letters, e.g.,
\verb9$\mathcal{A}$9; \\
$$ \mathcal{A} ~ \mathcal{B} ~ \mathcal{C} ~ \mathcal{D} ~
\mathcal{E} ~ \mathcal{F} ~ \mathcal{G} ~ \mathcal{R} ~
\mathcal{S} ~ \mathcal{T} ~ \mathcal{Z} $$

In number theory, various number sets are often
indicated using
\emph{blackboard capital} letters, e.g.,
\verb9$\mathbb{R}$9; \\
$$ \mathbb{N} ~ \mathbb{Z} ~ \mathbb{Z}_{17} ~
	~ \mathbb{R} ~ \mathbb{C} $$

Another style used now and then is
\emph{Fraktur} font letters, e.g.,
\verb9$\mathfrak{F}$9; \\
$$ \mathfrak{A} ~ \mathfrak{B} ~ \mathfrak{C} ~ \mathfrak{D} ~
\mathfrak{E} ~ \mathfrak{F} ~ \mathfrak{G} ~ \mathfrak{R} ~
\mathfrak{S} ~ \mathfrak{T} ~ \mathfrak{Z} $$

In vector algebra it is common to use \emph{boldface} letters
to indicate nonscalar values (vectors and matrices), e.g.,
\verb9$\mathbf{A}$9; \\
$$ \mathbf{A} ~ \mathbf{B} ~ \mathbf{C} ~ \mathbf{M} ~
\mathbf{v} ~ \mathbf{x} ~ \mathbf{y} ~ \mathbf{z} $$

One can get \emph{bold italic} letters for vector variables
by using, e.g.,
\verb9$\textbf{\em x}$9; \\
$$ \textbf{\em A B C D E F G s t u v w x y z} $$
";i:1;s:5:"latex";i:2;s:12:"appendix.tex";}i:2;i:4298;}i:90;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:91;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:5713;}i:92;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:3859;i:1;i:0;i:2;i:2;i:3;s:29:"How to make appendix in LaTeX";}i:2;i:5713;}i:93;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:5713;}}