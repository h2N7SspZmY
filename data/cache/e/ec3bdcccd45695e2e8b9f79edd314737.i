a:81:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1;}i:3;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:-1;i:1;i:0;i:2;i:1;i:3;s:0:"";}i:2;i:1;}i:4;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:5:"LaTeX";i:1;i:1;i:2;i:1;}i:2;i:1;}i:5;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:6;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:7;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:22;}i:8;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:1;i:1;i:21;i:2;i:1;i:3;s:5:"LaTeX";}i:2;i:22;}i:9;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:59:"How to put utf8 characters on listings environment in LaTeX";i:1;i:2;i:2;i:22;}i:2;i:22;}i:10;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:22;}i:11;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:979:"
\lstset{extendedchars=true,
	inputencoding=utf8,
	basicstyle=\small\color{blue},
	keywordstyle=\color{red}\bfseries,
	keywordstyle= [2]\color{magenta}\bfseries,
	identifierstyle=\color{blue},
	commentstyle=\color{gray}\textit,
	stringstyle=\color{darkGreen},
	showstringspaces=false,
	tabsize=2
}

\lstset{
	numbers=left,         % Números na margem da esquerda
	numberstyle=\tiny\color{darkGreen},    % Números pequenos
	stepnumber=1,         % De 1 em 1
	numbersep=5pt         % Separados por 5pt
}

\begin{lstlisting}[language=Shell, texcl=true, numbers=none, escapechar={\%}]
\end{lstlisting}

\lstinputlisting[firstline=11, lastline=17, language=Go, texcl=true, numbers=none, escapechar={\$}]{../pc_channel.go}
\lstinputlisting[firstline=18, lastline=22, language=Go, texcl=true, numbers=none, escapechar={\#}]{../pc_channel.go}

\begin{lstlisting}[language=Shell, texcl=true, numbers=none, breaklines, escapechar={\%}]
\end{lstlisting}

\lstinline[language=Go]|chan T|

";i:1;s:5:"latex";i:2;N;}i:2;i:100;}i:12;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:13;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1095;}i:14;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:22;i:1;i:1094;i:2;i:2;i:3;s:59:"How to put utf8 characters on listings environment in LaTeX";}i:2;i:1095;}i:15;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:58:"How to make a multicolumn / multirow tabular cell in LaTeX";i:1;i:2;i:2;i:1095;}i:2;i:1095;}i:16;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:1095;}i:17;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:161:"
\usepackage{multirow}
\begin{tabular}{|c|c|}
\hline
\multicolumn{2}{|c|}{Two Column} \\
\hline
\multirow{2}{*}{Two Row} & one \\
 & two \\
\hline
\end{tabular}
";i:1;s:5:"latex";i:2;N;}i:2;i:1172;}i:18;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1347;}i:19;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"Outputs:
";}i:2;i:1349;}i:20;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1358;}i:21;a:3:{i:0;s:10:"table_open";i:1;a:2:{i:0;i:2;i:1;i:3;}i:2;i:1358;}i:22;a:3:{i:0;s:13:"tablerow_open";i:1;a:0:{}i:2;i:1358;}i:23;a:3:{i:0;s:14:"tablecell_open";i:1;a:3:{i:0;i:2;i:1;s:6:"center";i:2;i:1;}i:2;i:1358;}i:24;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:14:"  Two Column  ";}i:2;i:1360;}i:25;a:3:{i:0;s:15:"tablecell_close";i:1;a:0:{}i:2;i:1374;}i:26;a:3:{i:0;s:14:"tablerow_close";i:1;a:0:{}i:2;i:1376;}i:27;a:3:{i:0;s:13:"tablerow_open";i:1;a:0:{}i:2;i:1376;}i:28;a:3:{i:0;s:14:"tablecell_open";i:1;a:3:{i:0;i:1;i:1;s:6:"center";i:2;i:2;}i:2;i:1376;}i:29;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:11:"  Two Row  ";}i:2;i:1378;}i:30;a:3:{i:0;s:15:"tablecell_close";i:1;a:0:{}i:2;i:1389;}i:31;a:3:{i:0;s:14:"tablecell_open";i:1;a:3:{i:0;i:1;i:1;s:6:"center";i:2;i:1;}i:2;i:1389;}i:32;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"  one  ";}i:2;i:1390;}i:33;a:3:{i:0;s:15:"tablecell_close";i:1;a:0:{}i:2;i:1397;}i:34;a:3:{i:0;s:14:"tablerow_close";i:1;a:0:{}i:2;i:1398;}i:35;a:3:{i:0;s:13:"tablerow_open";i:1;a:0:{}i:2;i:1398;}i:36;a:3:{i:0;s:14:"tablecell_open";i:1;a:3:{i:0;i:1;i:1;s:6:"center";i:2;i:1;}i:2;i:1405;}i:37;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"  two  ";}i:2;i:1406;}i:38;a:3:{i:0;s:15:"tablecell_close";i:1;a:0:{}i:2;i:1413;}i:39;a:3:{i:0;s:14:"tablerow_close";i:1;a:0:{}i:2;i:1414;}i:40;a:3:{i:0;s:11:"table_close";i:1;a:0:{}i:2;i:1414;}i:41;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:42;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1416;}i:43;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:1095;i:1;i:1415;i:2;i:2;i:3;s:58:"How to make a multicolumn / multirow tabular cell in LaTeX";}i:2;i:1416;}i:44;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:54:"How to fix "LaTeX Error: Too many unprocessed floats."";i:1;i:2;i:2;i:1416;}i:2;i:1416;}i:45;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:1416;}i:46;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:14:"
  \clearpage
";i:1;s:5:"latex";i:2;N;}i:2;i:1489;}i:47;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:48;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1519;}i:49;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:1416;i:1;i:1518;i:2;i:2;i:3;s:54:"How to fix "LaTeX Error: Too many unprocessed floats."";}i:2;i:1519;}i:50;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:25:"How use BibTeX with LaTeX";i:1;i:2;i:2;i:1519;}i:2;i:1519;}i:51;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:1519;}i:52;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1556;}i:53;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"
See ";}i:2;i:1557;}i:54;a:3:{i:0;s:12:"internallink";i:1;a:2:{i:0;s:4:"make";i:1;N;}i:2;i:1562;}i:55;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:6:". See ";}i:2;i:1570;}i:56;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:55:"http://www.kfunigraz.ac.at/~binder/texhelp/bibtx-7.html";i:1;s:26:"Help on BibTeX Entry types";}i:2;i:1576;}i:57;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:".";}i:2;i:1662;}i:58;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1663;}i:59;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:60;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1665;}i:61;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:1519;i:1;i:1664;i:2;i:2;i:3;s:25:"How use BibTeX with LaTeX";}i:2;i:1665;}i:62;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:26:"How to use AbnTeX in LaTeX";i:1;i:2;i:2;i:1665;}i:2;i:1665;}i:63;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:1665;}i:64;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:1134:"

\documentclass[a4paper,10pt]{abnt} %
\usepackage[utf8x]{inputenc}
\usepackage[brazil]{babel}
\usepackage{graphicx}
\usepackage{listings}
\usepackage[usenames,dvipsnames]{color}
\usepackage{verbatim, amssymb, latexsym, amsmath, mathrsfs}
\usepackage{url}
\usepackage{subfigure}
\usepackage{multicol}
\usepackage{framed}
\usepackage{array}
\usepackage{float}
\usepackage[section] {placeins}
\usepackage[table]{xcolor}

%opening
%\title{}
%\author{Kauê Silveira \\171671 \and Silveira Kauê \\ 671171}

\autor{Bruno Coswig Fiss \\ Felipe Augusto Chies \\ Kauê Soares da Silveira \\ Marcos Vinicius Cavinato} %\and
\instituicao{Universidade Federal do Rio Grande do Sul}
\orientador[Professor:]{Sérgio Felipe Zirbes}
\titulo{Especificação de um Sistema de Integração On-line Empresa / Representações para Pilaplast Ind. e Com. de Plásticos Ltda.}
\comentario{Disciplina INF01127 – Engenharia de Software N}
\data{1 de julho de 2010}

\begin{document}

%\maketitle
\folhaderosto
\sumario
\listadetabelas
\listadefiguras

%\begin{abstract}
%\begin{resumo}
%\end{abstract}
%\end{resumo}

\chapter{Introdução}

\end{document}
";i:1;s:5:"latex";i:2;N;}i:2;i:1710;}i:65;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:66;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:2860;}i:67;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:1665;i:1;i:2859;i:2;i:2;i:3;s:26:"How to use AbnTeX in LaTeX";}i:2;i:2860;}i:68;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:42:"How to define a macro (a command) in LaTeX";i:1;i:2;i:2;i:2860;}i:2;i:2860;}i:69;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:2860;}i:70;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:433:"
\newcommand{\bll}{\begin{lstlisting}}
\newcommand{\bx}[1]{\begin{#1}}
\newcommand{\bfr}{\begin{frame}}
\newcommand{\fr}[2]{\begin{frame}{#1}#2\end{frame}}
\newcommand{\bl}[2]{\begin{block}{#1}#2\end{block}}
\newcommand{\bb}[1]{\begin{block}{#1}}
\newcommand{\eb}{\end{block}}
\newcommand{\bi}{\begin{itemize}}
\newcommand{\ei}{\end{itemize}}
\newcommand{\ii}{\item}
\newcommand{\bc}{\begin{comment}}
\newcommand{\ec}{\end{comment}}
";i:1;s:5:"latex";i:2;N;}i:2;i:2921;}i:71;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:72;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:3370;}i:73;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:2860;i:1;i:3369;i:2;i:2;i:3;s:42:"How to define a macro (a command) in LaTeX";}i:2;i:3370;}i:74;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:39:"How to make a table of figures in LaTeX";i:1;i:2;i:2;i:3370;}i:2;i:3370;}i:75;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:3370;}i:76;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:345:"
\begin{figure}[ht]
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=50mm]{gui0.png}
		\caption{default}
		\label{fig:figure1}
	\end{minipage}
	\hspace{0.5cm}
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=50mm]{gui1.png}
		\caption{default}
		\label{fig:figure2}
	\end{minipage}
\end{figure}
";i:1;s:5:"latex";i:2;N;}i:2;i:3428;}i:77;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:78;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:3788;}i:79;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:3370;i:1;i:0;i:2;i:2;i:3;s:39:"How to make a table of figures in LaTeX";}i:2;i:3788;}i:80;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:3788;}}