a:35:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1;}i:3;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:-1;i:1;i:0;i:2;i:1;i:3;s:0:"";}i:2;i:1;}i:4;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:5:"Boost";i:1;i:1;i:2;i:1;}i:2;i:1;}i:5;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:6;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:20;}i:7;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:"
";}i:2;i:21;}i:8;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:21:"http://www.boost.org/";i:1;s:5:"Boost";}i:2;i:22;}i:9;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:": ";}i:2;i:53;}i:10;a:3:{i:0;s:13:"interwikilink";i:1;a:4:{i:0;s:8:"wp>Boost";i:1;N;i:2;s:2:"wp";i:3;s:5:"Boost";}i:2;i:55;}i:11;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:33:" - Portable C++ source libraries ";}i:2;i:67;}i:12;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:100;}i:13;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:14;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:102;}i:15;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:1;i:1;i:101;i:2;i:1;i:3;s:5:"Boost";}i:2;i:102;}i:16;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:20:"How to install Boost";i:1;i:2;i:2;i:102;}i:2;i:102;}i:17;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:102;}i:18;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:51:"
# download
$ ./bootstrap.sh
$ sudo ./bjam install
";i:1;s:4:"bash";i:2;N;}i:2;i:141;}i:19;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:20;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:207;}i:21;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:102;i:1;i:206;i:2;i:2;i:3;s:20:"How to install Boost";}i:2;i:207;}i:22;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:41:"How to build a simple program using Boost";i:1;i:2;i:2;i:207;}i:2;i:207;}i:23;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:207;}i:24;a:3:{i:0;s:4:"file";i:1;a:3:{i:0;s:271:"
#include <boost/lambda/lambda.hpp>
#include <iostream>
#include <iterator>
#include <algorithm>

int main()
{
    using namespace boost::lambda;
    typedef std::istream_iterator<int> in;

    std::for_each(
        in(std::cin), in(), std::cout << (_1 * 3) << " " );
}
";i:1;s:3:"c++";i:2;s:9:"boost.cpp";}i:2;i:267;}i:25;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:26;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:562;}i:27;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:207;i:1;i:561;i:2;i:2;i:3;s:41:"How to build a simple program using Boost";}i:2;i:562;}i:28;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:26:"How to compile using Boost";i:1;i:2;i:2;i:562;}i:2;i:562;}i:29;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:562;}i:30;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:36:"
$ gcc boost.cpp -o boost
$ ./boost
";i:1;s:4:"bash";i:2;N;}i:2;i:607;}i:31;a:2:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:7:"uparrow";i:1;a:1:{i:0;s:42:"lib/plugins/uparrow/images/tango-small.png";}i:2;i:1;i:3;s:6:"~~UP~~";}}i:32;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:659;}i:33;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:562;i:1;i:0;i:2;i:2;i:3;s:26:"How to compile using Boost";}i:2;i:659;}i:34;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:659;}}