~~SLIDESHOW~~

====== Séquence 1 ======

Intermédiaire

===== la concordance des temps =====

^ ce qui a été dit ^ ce qui a été rapporté ^ example ^^
| présent | -> imparfait | -> Tu **vas** bien? | -> Il a demande si tu **allais** bien. |
| futur | -> conditionnel | -> J'**arriverai** à 23h. | -> Il a dit qu'il **arriverait** à 23 h. |
| futur avec aller | -> aller à l'imparfait | -> Tu **vas inviter** son père? | -> Il a demandé si tu **allais inviter** son père. |
| passé composé | -> plus-que-parfait | -> J'**ai trouvé** du travail. | -> Il a dit qu'il **avait trouvé** du travail. |
| conditionnel | -> conditionnel | -> Tu **aimerais** la rencontrer. | -> Il a demandé si tu **aimerais** la rencontrer. |

===== le plus-que-parfait =====

| avoir | -> à l'imparfait | -> participe passé | -> J'**avais** oublié. |
| être | ::: | ::: | -> J'**étais** parti(e). |


to be continued ...