~~SLIDESHOW~~

====== Séquence 11 ======

Élémentaire

===== conditionnel: pouvoir =====

| je, tu | -> pourrais |
| il, elle | -> pourrait |
| nous | -> pourrions |
| vous | -> pourriez |
| ils, elles | -> pourraient |

===== conditionnel: voloir =====

| je, tu | -> voudrais |
| il, elle | -> voudrait |
| nous | -> voudrions |
| vous | -> voudriez |
| ils, elles | -> voudraient |

===== conditionnel: aimer =====

| je, tu | -> aimerais |
| il, elle | -> aimerait |
| nous | -> aimerions |
| vous | -> aimeriez |
| ils, elles | -> aimeraient |

===== conditionnel: avoir =====

| je, tu | -> aurais |
| il, elle | -> aurait |
| nous | -> aurions |
| vous | -> auriez |
| ils, elles | -> auraient |

===== demande polie =====

| Bonjour |
| Je voudrais ... |
| Merci |
| Au revoir |

===== le pronom on =====

| nous (langue parlée) |
| quelqu'un (une personne indéterminée) |
| le gens en géneral (la population) |

===== adjectifs =====

| poli |
| impoli |
| aimable |
| franc |
| gentil |
| agressif |
| sévère |
| méchant |

===== les relatifs qui / que =====

| Marie **aime** le travail bie fait. |
| Marie, c'est une fille **qui aime** le travail bien fait. |
| J'**aime** beaucoup Marie. |
| Marie, c'est une fille **que j'aime** beaucoup. |

===== formation du futur =====

| manger | -> je **mange** | -> je **mange**rai |
| parler | -> je **parle** | -> je **parle**rai |

===== future: arriver =====

| j' | -> arriverai |
| tu | -> arriveras |
| il, elle | -> arrivera |
| nous | -> arriverons |
| vous | -> arriverez |
| ils, elles | -> arriveront |

===== future: manger =====

| je | -> mangerai |
| tu | -> mangeras |
| il, elle | -> mangera |
| nous | -> mangerons |
| vous | -> mangerez |
| ils, elles | -> mangeront |

===== future: parler =====

| je | -> parlerai |
| tu | -> parleras |
| il, elle | -> parlera |
| nous | -> parlerons |
| vous | -> parlerez |
| ils, elles | -> parleront |

===== future: être =====

| je | -> serai |
| tu | -> seras |
| il, elle | -> sera |
| nous | -> serons |
| vous | -> serez |
| ils, elles | -> seront |

===== future: avoir =====

| j' | -> aurai |
| tu | -> auras |
| il, elle | -> aura |
| nous | -> aurons |
| vous | -> aurez |
| ils, elles | -> auront |

===== bulletin météo =====

| neige |
| nuages |
| orage |
| pluie |

