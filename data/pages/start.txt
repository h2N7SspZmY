====== Kauê Silveira ======

{{kaue.jpg |Kauê Silveira}}
 
I'm an undergraduate student of [[http://www.inf.ufrgs.br/|Computer Science]] at [[http://www.ufrgs.br/|UFRGS]] (Federal University of Rio Grande do Sul), Brazil. I'm currently a member of [[http://www.inf.ufrgs.br/pet/|PET-Computing Group]].

[[http://lattes.cnpq.br/5154068006247589|Curriculum]] \\
[[http://kaflubes.blogspot.com/|Blog]] \\
[[http://www.orkut.com.br/Main#Profile?uid=15939730725879078384|Orkut]] \\
[[http://www.topcoder.com/tc?module=MemberProfile&cr=22843857|TopCoder]] \\
[[http://www.hacker.org/forum/profile.php?mode=viewprofile&u=6969|Hacker.org]] \\
[[http://www.twitter.com/kssilveira|Twitter]] \\
{{email.png? 162 x 100|Please feel free to contact me if you have any question or comments!}}

----

~~CLOUD~~
<!-- ~~TAGCLOUD~~ -->

====== Last Changes ======
{{changes>render = list(signature, summary)&5}}

====== Quotation ======

{{xfortune>cookies.pdf?3600}}

====== Friends ======

[[http://umblag.wordpress.com/|Blag]] \\
[[http://www.inf.ufrgs.br/~bjurkovski/|Bruno Jurkovski]] \\
[[http://www.inf.ufrgs.br/~fachies/|Felipe Augusto Chies]] \\
[[http://www.inf.ufrgs.br/~lfzawacki/|Lucas Fialho Zawacki]] \\
[[http://linesocode.wordpress.com/|Lines of Code]] \\
