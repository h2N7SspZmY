====== Diff ======

===== How to compare two folders recursively =====

<code bash>
# Comparing two folder recursively, paginating, output in two columns, reporting identical files.
$ diff -r -l -y -s path/to/folder/a path/to/folder/b | less
</code>

{{tag>programming}}