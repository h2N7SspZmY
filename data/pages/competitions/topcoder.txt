====== TopCoder ======

[[http://www.topcoder.com/|TopCoder]]: [[wp>TopCoder]]

===== How to install the TopCoder Algorithm Matches arena plug-ins =====

[[http://www.topcoder.com/wiki/display/tc/How+to+install+The+Arena+plug-ins|Tutorial]] \\

===== TopCoder Algorithm Matches FileEdit C++ Template =====

<code cpp>
#include <algorithm>
#include <bitset>
#include <cassert>
#include <cctype>
#include <cfloat>
#include <climits>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <list>
#include <map>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <streambuf>
#include <string>
#include <utility>
#include <vector>

#define rep(i, n) repb(i, 0, n)
#define repb(i, b, n) repbc(i, b, n, <)
#define repe(i, n) repbe(i, 0, n)
#define repbe(i, b, n) repbc(i, b, n, <=)
#define repbc(i, b, n, c) for(int i = b; i c n; i++)

using namespace std;

class $CLASSNAME$ {
	public:
	$RC$ $METHODNAME$($METHODPARMS$) {
		$RC$ res;

		

		return res;
	}
	$TESTCODE$
};

// BEGIN CUT HERE 
int main()
{
	$CLASSNAME$ ___test; 
	___test.run_test(-1); 
} 
// END CUT HERE 
</code>

===== How to keep track of your time on TopCoder Marathon Matches =====

Look at [[http://forums.topcoder.com/?module=Thread&threadID=642239&start=0|this]] thread.

===== How To Generate TopCoder Algorithm Matches Statistics =====

[[http://ahmed-aly.selfip.com/TopCoderTools/|TopCoder Tools]] \\


===== My TopCoder Performances =====

[[http://www.topcoder.com/tc?module=MemberProfile&cr=22843857|kauesilv]] \\

{{tag>}}