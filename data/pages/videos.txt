====== Videos ======

[[http://brightcove.newscientist.com/services/player/bcpid1873822884?bctid=77464563001|Hand-held projector images respond to the real world ]] \\
[[http://www.youtube.com/watch?v=yplX3pYWlPo|Steve Jobs at Stanford (part 1)]] \\
[[http://www.youtube.com/watch?v=ksoo-G_YB2o|Steve Jobs at Stanford (part 2)]] \\
[[http://www.groklaw.net/staticpages/index.php?page=GatesDepo|Groklaw: Gates Deposition Video]] \\
[[http://www.youtube.com/watch?v=PsPX0nElJ0k|Code Bubbles: Rethinking the User Interface Paradigm of Integrated Development Environments]] \\
[[http://www.youtube.com/watch?v=Q5k7a9YEoUI|SCRUM in Under 10 Minutes]] \\
[[http://10gui.com/video/|10/GUI Multi-touch Operational System]] \\
[[http://www.youtube.com/watch?v=IzXVBlQ6wFY|CHI: Multitoe project offers new touch interface]] \\
<!-- [[|]] -->

===== Google Tech Talk =====

[[http://www.youtube.com/watch?v=4XpnKHJAok8|Linus Torvalds: Git]] \\
[[http://www.youtube.com/watch?v=rKnDgT73v8s|Rob Pike: The Go Programming Language]] \\
[[http://video.google.com/videoplay?docid=810232012617965344|Rob Pike: Advanced Topics in Programming Languages: Concurrency and Message Passing In Newsqueak]] \\
<!-- [[|]] -->

===== Authors@Google =====

[[http://www.youtube.com/watch?v=DZ2vtQCESpk|Garr Reynolds]] \\
<!-- [[|]] -->

===== TED: Ideas Worth Spreading =====

[[http://www.ted.com/talks/mark_roth_suspended_animation.html|Mark Roth: Suspended animation is within our grasp]] \\
[[http://www.ted.com/talks/bobby_mcferrin_hacks_your_brain_with_music.html|Bobby McFerrin hacks your brain with music]] \\
<!-- [[|]] -->
 
===== Music =====

[[http://www.gearwire.com/korg-kaossilatorpro-demo.html|Korg Kaossilator Pro]] \\
[[http://www.youtube.com/watch?v=98ew0VtHmik|Super Mario Castle Theme Death Metal]] \\
[[http://www.youtube.com/watch?v=9UagxOZ-DDQ|Learning How To Play Guitar Well Enough To Get You Laid]] \\
<!-- [[|]] -->

{{tag>links}}