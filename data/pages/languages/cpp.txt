====== C++ ======

[[wp>C++]]

===== How to use exceptions =====

<code c++>
#include <iostream>
using namespace std;

int f(int x)
{
	if(x < 0) throw 0;
	return 1;
}

int main()
{
	try {
		cout << f(0) << endl;
		cout << f(-1) << endl;
	} catch(int e) {
		cout << "catch " << e << endl;
	}
}
</code>
