====== LaTeX ======

[[http://www.latex-project.org/|LaTeX]]: [[wp>LaTeX|LaTeX]]

===== How use BibTeX with LaTeX =====

See [[:tools:make]]. See [[http://www.kfunigraz.ac.at/~binder/texhelp/bibtx-7.html|Help on BibTeX Entry types]].

===== How to put utf8 characters on listings environment in LaTeX =====

<code latex>
\lstset{extendedchars=true,
	inputencoding=utf8,
	basicstyle=\small\color{blue},
	keywordstyle=\color{red}\bfseries,
	keywordstyle= [2]\color{magenta}\bfseries,
	identifierstyle=\color{blue},
	commentstyle=\color{gray}\textit,
	stringstyle=\color{darkGreen},
	showstringspaces=false,
	tabsize=2
}

\lstset{
	numbers=left,         % Números na margem da esquerda
	numberstyle=\tiny\color{darkGreen},    % Números pequenos
	stepnumber=1,         % De 1 em 1
	numbersep=5pt         % Separados por 5pt
}

\begin{lstlisting}[language=Shell, texcl=true, numbers=none, escapechar={\%}]
\end{lstlisting}

\lstinputlisting[firstline=11, lastline=17, language=Go, texcl=true, numbers=none, escapechar={\$}]{../pc_channel.go}
\lstinputlisting[firstline=18, lastline=22, language=Go, texcl=true, numbers=none, escapechar={\#}]{../pc_channel.go}

\begin{lstlisting}[language=Shell, texcl=true, numbers=none, breaklines, escapechar={\%}]
\end{lstlisting}

\lstinline[language=Go]|chan T|

</code>

===== How to make a multicolumn / multirow tabular cell in LaTeX =====

<code latex>
\usepackage{multirow}
\begin{tabular}{|c|c|}
\hline
\multicolumn{2}{|c|}{Two Column} \\
\hline
\multirow{2}{*}{Two Row} & one \\
 & two \\
\hline
\end{tabular}
</code>

Outputs:

|  Two Column  ||
|  Two Row  |  one  |
| ::: |  two  |

===== How to fix "LaTeX Error: Too many unprocessed floats." =====

<code latex>
  \clearpage
</code>



===== How to use AbnTeX in LaTeX =====

<code latex>

\documentclass[a4paper,10pt]{abnt} %
\usepackage[utf8x]{inputenc}
\usepackage[brazil]{babel}
\usepackage{graphicx}
\usepackage{listings}
\usepackage[usenames,dvipsnames]{color}
\usepackage{verbatim, amssymb, latexsym, amsmath, mathrsfs}
\usepackage{url}
\usepackage{subfigure}
\usepackage{multicol}
\usepackage{framed}
\usepackage{array}
\usepackage{float}
\usepackage[section] {placeins}
\usepackage[table]{xcolor}

%opening
%\title{}
%\author{Kauê Silveira \\171671 \and Silveira Kauê \\ 671171}

\autor{Bruno Coswig Fiss \\ Felipe Augusto Chies \\ Kauê Soares da Silveira \\ Marcos Vinicius Cavinato} %\and
\instituicao{Universidade Federal do Rio Grande do Sul}
\orientador[Professor:]{Sérgio Felipe Zirbes}
\titulo{Especificação de um Sistema de Integração On-line Empresa / Representações para Pilaplast Ind. e Com. de Plásticos Ltda.}
\comentario{Disciplina INF01127 – Engenharia de Software N}
\data{1 de julho de 2010}

\begin{document}

%\maketitle
\folhaderosto
\sumario
\listadetabelas
\listadefiguras

%\begin{abstract}
%\begin{resumo}
%\end{abstract}
%\end{resumo}

\chapter{Introdução}

\end{document}
</code>

===== How to define a macro (a command) in LaTeX =====

<code latex>
\newcommand{\bll}{\begin{lstlisting}}
\newcommand{\bx}[1]{\begin{#1}}
\newcommand{\bfr}{\begin{frame}}
\newcommand{\fr}[2]{\begin{frame}{#1}#2\end{frame}}
\newcommand{\bl}[2]{\begin{block}{#1}#2\end{block}}
\newcommand{\bb}[1]{\begin{block}{#1}}
\newcommand{\eb}{\end{block}}
\newcommand{\bi}{\begin{itemize}}
\newcommand{\ei}{\end{itemize}}
\newcommand{\ii}{\item}
\newcommand{\bc}{\begin{comment}}
\newcommand{\ec}{\end{comment}}
</code>

===== How to make a table of figures in LaTeX =====

<code latex>
\begin{figure}[ht]
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=50mm]{gui0.png}
		\caption{default}
		\label{fig:figure1}
	\end{minipage}
	\hspace{0.5cm}
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=50mm]{gui1.png}
		\caption{default}
		\label{fig:figure2}
	\end{minipage}
\end{figure}
</code>

===== How to make appendix in LaTeX =====

<file latex main.tex>
\documentclass[a4paper,10pt]{report}
\usepackage{amssymb} %for math symbols in appendix.tex
\begin{document}
\tableofcontents
\chapter{One}
\appendix
%\addcontentsline{toc}{chapter}{Appendix}
\addtocontents{toc}{\protect\contentsline{chapter}{Appendix:}{}}
\chapter{Questionnaire Requirement}
\chapter{Questionnaire Testing}
\input appendix.tex
\end{document}
</file>

<file latex appendix.tex>
\section{Math font styles}			% file "appendix.tex"

Several extra font styles are available in math mode,
since there are so many typographical conventions
in various branches of mathematics.\footnote{By the way,
this is an appendix!}

In set theory (among other fields) one often uses
\emph{calligraphic} letters, e.g.,
\verb9$\mathcal{A}$9; \\
$$ \mathcal{A} ~ \mathcal{B} ~ \mathcal{C} ~ \mathcal{D} ~
\mathcal{E} ~ \mathcal{F} ~ \mathcal{G} ~ \mathcal{R} ~
\mathcal{S} ~ \mathcal{T} ~ \mathcal{Z} $$

In number theory, various number sets are often
indicated using
\emph{blackboard capital} letters, e.g.,
\verb9$\mathbb{R}$9; \\
$$ \mathbb{N} ~ \mathbb{Z} ~ \mathbb{Z}_{17} ~
	~ \mathbb{R} ~ \mathbb{C} $$

Another style used now and then is
\emph{Fraktur} font letters, e.g.,
\verb9$\mathfrak{F}$9; \\
$$ \mathfrak{A} ~ \mathfrak{B} ~ \mathfrak{C} ~ \mathfrak{D} ~
\mathfrak{E} ~ \mathfrak{F} ~ \mathfrak{G} ~ \mathfrak{R} ~
\mathfrak{S} ~ \mathfrak{T} ~ \mathfrak{Z} $$

In vector algebra it is common to use \emph{boldface} letters
to indicate nonscalar values (vectors and matrices), e.g.,
\verb9$\mathbf{A}$9; \\
$$ \mathbf{A} ~ \mathbf{B} ~ \mathbf{C} ~ \mathbf{M} ~
\mathbf{v} ~ \mathbf{x} ~ \mathbf{y} ~ \mathbf{z} $$

One can get \emph{bold italic} letters for vector variables
by using, e.g.,
\verb9$\textbf{\em x}$9; \\
$$ \textbf{\em A B C D E F G s t u v w x y z} $$
</file>

