~~SLIDESHOW~~

====== To Do ======

Things to do.

===== =====

  * <todo #>terminar de copiar meu site</todo> {{skill>2/5}} 
  * <todo #>mostrar quotation do dia</todo> {{skill>5/5}}
  * tutorial {{skill>4/5}}
    * <todo>vim</todo>
    * <todo>ctags</todo>
    * <todo>git</todo>
    * <todo>doxygen</todo>
    * <todo>make</todo>
    * <todo>app engine</todo>

===== =====

  * versionar a wiki {{skill>1/5}}
    * <todo #> git (abril) </todo>
    * <todo> dropbox (abril) </todo>
  * usar plugins {{skill>3/5}}
    * <todo>s5</todo>
    * <todo>skill</todo>
    * <todo>tag</todo>
  * <todo #> gerar estati´sticas (abril) </todo>

===== =====

  * <todo> cursos do pet no S5 </todo>
  * <todo> usar links para [[wp>wikipedia]] </todo>
  * <todo #> http://www.dokuwiki.org/plugin:xfortune </todo>
  * <todo>fazer uma página para cada tópico</todo>
  * <todo>atualizar charts</todo>
  * <todo> vim quick reference </todo>

===== =====

  * <todo>restate my assumptions: 1. Mathematics is the language of nature. 2. Everything around us can be represented and understood through numbers. 3. If you graph these numbers, patterns emerge. Therefore: There are patterns everywhere in nature. </todo>
  * <todo>página com python do appengine</todo>
  * <todo> começar os títulos das seções com 'How to' </todo>
  * <todo>http://www.dokuwiki.org/seo url rewriting</todo>