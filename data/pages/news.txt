====== News ======

===== 2010 =====

==== April ====

[[http://www.nytimes.com/2010/04/20/technology/20google.html|Cyberattack on Google Said to Hit Password System]] \\
[[http://www.ns.umich.edu/htdocs/releases/story.php?id=7633|Cat brain: A step toward the electronic equivalent]] \\
[[http://www2.cnrs.fr/en/1725.htm|Deciphering the movement of pedestrians in a crowd]] \\
[[http://www.pcworld.com/article/194021/multitoe_project_offers_new_touch_interface.html|Multitoe Project Offers New Touch Interface]] \\
[[http://www.physorg.com/news189868063.html|Online e-Expo Features More Than 100 University Robotics Labs]] \\
[[http://www.physorg.com/news189919411.html|'Mind-Reading' Brain-Scan Software Showcased in NY]] \\
[[http://www.nytimes.com/2010/04/11/business/global/11russia.html|Innovation, by Order of the Kremlin]] \\
[[http://tigger.uic.edu/htbin/cgiwrap/bin/newsbureau/cgi-bin/index.cgi?from=Releases&to=Release&id=2843&start=1263048276&end=1270824276&topic=0&dept=0|Battling Botnets With an Awesome OS]] \\
[[http://www.networkworld.com/news/2010/040910-steganography-data-loss.html|Steganography Discovery Could Help Data Thieves, But Also Improve Radar, Sonograms]] \\
[[http://cordis.europa.eu/ictresults/index.cfm?section=news&tpl=article&BrowsingType=Features&ID=91251|Augmented Reality Brings Movie Magic to City Visits]] \\
[[http://web.mit.edu/newsoffice/2010/ai-unification.html|A grand unified theory of AI]] \\
<!-- [[|]] -->

{{tag>links}} 